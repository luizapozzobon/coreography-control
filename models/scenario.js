const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const ScenarioSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  creator: {
    type: String,
    required: true
  },
  items: {
    type: Array
  },
  description: String
});

const Scenario = module.exports = mongoose.model('Scenario', ScenarioSchema);

module.exports.saveScenario = function(newScenario, callback) {
    newScenario.save(callback());
  }
