const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const SpeechSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  text: String,
  language: String,
  voice_name: String,
  voice_index: Number,
  creator: {
    type: String,
    required: true,
  }
});

const Speech = module.exports = mongoose.model('Speech', SpeechSchema);

module.exports.saveSpeech = function(newSpeech, callback) {
  newSpeech.save(callback());
}
