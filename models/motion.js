const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

const MotionSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  poses: Array,
  creator: {
    type: String,
    required: true,
  },
  duration: Number
});

const Motion = module.exports = mongoose.model('Motion', MotionSchema);

module.exports.getMotionById = function(id, callback) {
  Motion.findById(id, callback());
}

module.exports.getMotionByCreator = function(creator, callback) {
  const query = {creator: creator};
  Motion.findOne(query, callback());
}

module.exports.saveMotion = function(newMotion, callback) {
  newMotion.save(callback());
}
