const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Motion = require('../models/motion');
const pickle = require('pickle');


// Save new motion JSON file
router.post('/save', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  let newMotion = new Motion({
    name: req.body.name,
    poses: req.body.poses,
    creator: req.body.creator,
    duration: req.body.duration
  });

  Motion.saveMotion(newMotion, (err, motion) => {
    if(err) {
      res.json({success: false, msg: 'Failed to save motion'})
    } else {
      res.json({success: true, msg:'Motion saved.', data: newMotion})
    }
  })
});

router.patch('/:file', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  let updatedMotion = {$set:{
      poses: req.body.poses,
      creator: req.body.creator,
      duration: req.body.duration
  }};
  const query = {name: req.body.name};

  // authorization to update
  if (req.body.creator == req.body.old_creator || req.body.isAdmin == true) {
    Motion.findOneAndUpdate(query, updatedMotion, (err, doc) => {
      if(err){
        throw err;
      }

      if(!doc) {
        return res.json({success: false, msg: 'No data received', name: query, motion: updatedMotion})
      }

      res.json({success: true, data:doc, newMotion: updatedMotion});
    });
  } else {
    return res.json({success: false, motion: updatedMotion, msg: 'In order to save this file you should be the original creator or Admin.'})
  }
})

// Open existing motion file
router.get('/:file/edit', passport.authenticate('jwt', {session:false}), (req, res, next) => {

  const name = {name: req.params.file};

  Motion.findOne(name, (err, motion) => {
    if(err) throw err;

    if(!motion){
      return res.json({success: false, msg: 'Motion not found', name: name});
    } else {

    }
    res.json({data: motion});
  });
})

router.get('/:file/find', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const query = {name: req.params.file};

  Motion.findOne(query, (err, motion) => {
    if (motion) {
      return res.json({success: true, msg: 'Motion found', creator: motion.creator});
    } else if(!motion){
      return res.json({success: false, msg: 'Motion not found', name: req.params.file});
    }
  });

})

router.get('/list', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  Motion.find({}, 'name creator duration', (err, list) => {
  // Motion.find( (err, list) => {
    if (err){
      res.send(err);
    }
    res.json(list);
  })
})

router.post('/list/search', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const query = {name: {$regex : req.body.search}}

  Motion.find(query, (err, searchList) => {
    if (err) {
      res.send(err);
    }
    res.json({data: searchList, query: req.body.seach});
  })
})

router.get('/:username/quantity', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const query = {creator: req.params.username};

  Motion.count(query, (err, data) => {
    if (err) { res.send(err); }

    if (data) {
      res.json({data: data});
    }
  })
})

router.delete('/:file', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  const name = {name: req.params.file};

  Motion.findOneAndRemove(name, (err, data) => {
    if (err) throw err;

    if (!data) {
      return res.json({success: false, msg: 'No data retrieved', name: name});
    }
    res.json({data: data});
  })
})

router.get('/:file/download', passport.authenticate('jwt', {session:false}), (req, res, next) => {

    const name = {name: req.params.file};

    Motion.findOne(name, (err, motion) => {
      if(err) throw err;

      if(!motion){
        return res.json({success: false, msg: 'Motion not found', name: name});
      }

      pickle.dumps(motion.poses, function(pickled) {
        res.json({data: pickled})
      });

      // res.json({data:motion});
    });
  })

  router.post('/upload', (req, res, next) => {
    var pickled = req.body.poses

    pickle.loads(pickled, function(original)
    {
      var motion = original;

      let newMotion = new Motion({
        name: req.body.name,
        poses: motion,
        creator: req.body.creator
      });

      const name = {name: req.body.name};

      Motion.findOne(name, (err, motion) => {
        if (err) throw err;

        if (!motion) {
          Motion.saveMotion(newMotion, (err, motion) => {
            if(err) {
              return res.json({success: false, msg: 'Failed to save motion'});
            } else {
              return res.json({success: true, msg:'Motion saved.', data: newMotion});
            }
          })
        } else {
          return res.json({success: false, msg: 'Motion already exists'});
        }
      })
    });
  })

module.exports = router;
