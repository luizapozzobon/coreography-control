const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Scenario = require('../models/scenario');
const pickle = require('pickle');

router.post('/save', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let newScenario = new Scenario({
        name: req.body.name,
        items: req.body.items,
        creator: req.body.creator,
        description: req.body.description
      });

      Scenario.saveScenario(newScenario, (err, scenario) => {
        console.log('aqui', scenario)
        if(err) {
          res.json({success: false, msg: 'Failed to save scenario'})
        } else {
          res.json({success: true, msg:'Scenario saved.', data: newScenario})
        }
      })
});

router.patch('/:file', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  console.log(req.body)
  let updatedScenario = {$set:{
      items: req.body.items,
      creator: req.body.creator,
      description: req.body.description
  }};
  const query = {name: req.body.name};

  // authorization to update
  if (req.body.creator == req.body.old_creator || req.body.isAdmin == true) {
    Scenario.findOneAndUpdate(query, updatedScenario, (err, doc) => {
      if(err){
        throw err;
      }

      if(!doc) {
        return res.json({success: false, msg: 'No data received', name: query, scenario: updatedScenario})
      }

      res.json({success: true, data:doc, newScenario: updatedScenario});
    });
  } else {
    return res.json({success: false, motion: updatedScenario, msg: 'In order to save this file you should be the original creator or Admin.'})
  }
})

router.get('/list', passport.authenticate('jwt', {session:false}), (req, res, next) => {

    Scenario.find({}, 'name creator description', (err, list) => {
    // Scenario.find( (err, list) => {
        if (err){
        res.send(err);
        }
        res.json(list);
    })
})

router.post('/list/search', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const query = {name: {$regex : req.body.search}}

  Scenario.find(query, (err, searchList) => {
    if (err) {
      res.send(err);
    }
    res.json({data: searchList, query: req.body.seach});
  })
})

router.get('/:file/edit', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const query = {name: req.params.file};
  console.log('find', query)
  Scenario.findOne(query, (err, scenario) => {
    if (scenario) {
      return res.json({success: true, msg: 'Scenario found', creator: scenario.creator, scenario: scenario});
    } else if(!scenario){
      return res.json({success: false, msg: 'Scenario not found', name: req.params.file});
    }
  });

})

router.get('/:username/quantity', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  const query = {creator: req.params.username};

  Scenario.count(query, (err, data) => {
    if (err) { res.send(err); }

    if (data) {
      res.json({data: data});
    }
  })
})

router.delete('/:file', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  const name = {name: req.params.file};

  Scenario.findOneAndRemove(name, (err, data) => {
    if (err) throw err;

    if (!data) {
      return res.json({success: false, msg: 'No data retrieved', name: name});
    }
    res.json({data: data});
  })
})


router.post('/download', passport.authenticate('jwt', {session:false}), (req, res, next) => {

  const query = {items: req.body};
  console.log(req.body)

  pickle.dumps(query.items, function(pickled) {
    res.json({data: pickled})
  });
})

module.exports = router;
