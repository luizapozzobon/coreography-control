const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Speech = require('../models/speech');
const mongoose = require('mongoose');
const GridFs = require('gridfs-stream');
const fs = require('fs');
const streamifier = require('streamifier');
const tempy = require('tempy');
const gTTS = require('gtts');
const sox = require('sox-stream');
const deasync = require('deasync');

// var db = mongoose.connection;
db = '';

var getConnDb = function() {
  var connDb;
  mongoose.connection.then(function(conn) {
    connDb = conn.db;
  });
  while (connDb === undefined) {
    deasync.runLoopOnce();
  }
  db = connDb;
  return connDb;
}

var mongoDriver = mongoose.mongo;
var gfs = new GridFs(getConnDb(), mongoDriver);

router.get('/', passport.authenticate('jwt', {session: false}), (req, res, next) => {

    gfs.files.find().toArray(function (err, files) {
        if (err) { throw new Error(err); }
        return res.json({success: true, list: files})
    })

})

// Gera speech em diversas linguagens
router.post('/generate', passport.authenticate('jwt', {session:false}), (req, res, next) => {

    let data = req.body;
    console.log(data)

    if (data.info == undefined) {
      data.info = {}
      if (data.language.lang == 'pt-br') {
        data.info.speed = 1.25;
        data.info.pitch = 150;
      } else if (data.language.lang == 'en') {
        data.info.speed = 1.2;
        data.info.pitch = 150;
      } else if (data.language.lang === 'de') {
        data.info.speed = 1.1;
        data.info.pitch = 120;
      } else if (data.language.lang === 'fr') {
        data.info.speed = 1.1;
        data.info.pitch = 120;
      } else if (data.language.lang === 'es') {
        data.info.speed = 1.1;
        data.info.pitch = 105;
      }
      console.log('with standard info', data)
    }

    var gtts = new gTTS(data.text, data.language.lang);
    tempfile = tempy.file({name: 'temp.mp3'})

    gtts.save(tempfile, function (err, result) {
        if(err) { throw new Error(err) }

        var writestream = gfs.createWriteStream({
            filename: data.name,
            content_type: 'audio/mp3',
            metadata: {
                creator: data.creator,
                type: 'speech',
                text: data.text,
                language: data.language.lang
            }
        });

        if(data.language.lang === 'en') {
            var transform = sox({
                input: {type: 'mp3'},
                output: {type: 'mp3'},
                effects: ['speed', data.info.speed,
                        'pitch', data.info.pitch,
                        'echo', 0.8, 0.88, 60, 0.4 ]
            })
        } else if(data.language.lang === 'pt-br' || data.language.lang === 'de'
                  || data.language.lang === 'fr' || data.language.lang === 'es') {
            var transform = sox({
                input: {type: 'mp3'},
                output: {type: 'mp3'},
                effects: ['speed', data.info.speed,
                        'pitch', data.info.pitch]
            })
        }

        try {
          fs.createReadStream(tempfile)
          .pipe(transform)
          .pipe(writestream);
        } finally {
          console.log('Readstream done')
        }

        writestream.on('close', function (file) {
            // do something with `file`
            console.log(file.filename + ' Written To DB');
            console.log(file)
            return res.json({'result': 'finished', data: file});
        });
    });
})

// Live speech
router.get('/live', passport.authenticate('jwt', {session: false}), (req, res, next) => {

  let data = JSON.parse(JSON.stringify(req.query));
  console.log(data);
  var gtts = new gTTS(data.text, data.lang);

  db.collection('fs.files')
  .find({filename: 'temp_speech'})
  .toArray(function(err, files) {
      if (err) throw err;

      if(files.length > 0) {
          files.forEach(function(file) {
              gfs.remove({_id: file._id}, function (err, gridStore) {
                  if(err) { throw new Error(err); }
                  console.log('removed')
                });
          });
      }

      var gtts = new gTTS(data.text, data.lang);
      tempfile = tempy.file({name: 'temp.wav'})

      gtts.save(tempfile, function (err, result) {
          if(err) { throw new Error(err) }

          var writestream = gfs.createWriteStream({
              filename: 'temp_speech',
              content_type: 'audio/mp3',
              metadata: {
                  creator: 'temp',
                  type: 'speech',
                  text: data.text,
                  language: data.lang
              }
          });

          if(data.lang === 'en') {
              var transform = sox({
                  input: {type: 'mp3'},
                  output: {type: 'mp3'},
                  effects: ['speed', 1.20,
                          'pitch', 105]
              })
          } else if(data.lang === 'pt-br') {
              var transform = sox({
                  input: {type: 'mp3'},
                  output: {type: 'mp3'},
                  effects: ['speed', 1.25,
                          'pitch', 105]
              })
          }

          fs.createReadStream(tempfile)
              .pipe(transform)
              .pipe(writestream);

          writestream.on('close', function (file) {
              // do something with `file`
              console.log(file.filename + ' Written To DB');
              console.log(file)

              return res.json({'result': 'finished', data: file});
          });
      });
  });
})

router.delete('/:speechID', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let query = req.params.speechID;

    gfs.remove({_id: query}, function (err, gridStore) {
        if(err) { throw new Error(err); }
        return res.json({success: true});
      });
})

router.get('/:speechID/play', passport.authenticate('jwt', {session: false}), (req, res, next) => {

    const query = req.params.speechID;

    gfs.exist({ _id: query }, function (err, file) {
        if (err || !file) {
            res.send('File Not Found');
        } else {
            var readstream = gfs.createReadStream({
                _id: query
            });

            res.setHeader('content-type', 'audio/mpeg');

            readstream.on('data', function(data) {
                res.write(JSON.stringify(data));
            });

            readstream.on('end', function() {
                res.end();
            });

            readstream.on('error', function (err) {
                console.log('An error occurred!', err);
                throw err;
            });
        }
    });
})


router.get('/:username/quantity', passport.authenticate('jwt', {session: false}), (req, res, next) => {

    let query = {"metadata": {"creator": req.params.username}};
    console.log(query);
    let count = 0;

    gfs.files.find({}).toArray(function (err, files) {
        if (err) { throw new Error(err); }
        files.forEach(function(file) {
            if(file.metadata.creator === req.params.username) {
                count++;
            }
        })
        return res.json({success: true, quantity: count});
    })
})

// nem é usado
router.post('/adjust', passport.authenticate('jwt', {session: false}), (req, res, next) => {

    let info = req.body;
    console.log(info);

    gfs.findOne({ _id: info.speechID }, function (err, file) {

        var readstream = gfs.createReadStream({
            _id: info.speechID
        });

        readstream.on('data', function(data) {
            var buffer = data

            var writestream = gfs.createWriteStream({
                filename: file.filename + '_adjusted',
                content_type: 'audio/mpeg',
                metadata: {
                    creator: file.metadata.creator,
                    type: file.metadata.type,
                    text: file.metadata.text
                }
            });

            var transform = sox({
                input: {type: 'mp3'},
                output: {type: 'mp3'},
                effects: ['speed', 1.15,
                        'pitch', 2,
/*                         'echo', 0.8, 0.88, 60, 0.4  */]
            })

            streamifier.createReadStream(buffer)
                .pipe(transform)
                .pipe(writestream);
        })

        readstream.on('error', function (err) {
            console.log('An error occurred!', err);
            throw err;
        });
    });
});

module.exports = router;
