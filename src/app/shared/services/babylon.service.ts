import { Injectable } from '@angular/core';

import * as BABYLON from 'babylonjs';

@Injectable()
export class BabylonService {
  scene;
  engine;
  camera;
  light;
  createScene;

  constructor() { }

  onLoad() {
    let canvas = document.getElementById('renderCanvas') as HTMLCanvasElement;
    canvas.height = 660;
    canvas.width = 500;

      this.engine = new BABYLON.Engine(canvas, true,  { stencil: true });

      this.createScene = function(){

        this.scene = new BABYLON.Scene(this.engine);

        this.camera = new BABYLON.ArcRotateCamera('Camera', 3 * Math.PI / 2, Math.PI / 3, 520, new BABYLON.Vector3(0, 0, 0), this.scene);

        this.camera.attachControl(canvas, true);

        this.light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 2, 0), this.scene);

        this.scene.clearColor = new BABYLON.Color4(0, 0, 0, 0.0000000001);

        return this.scene;
      };

    this.scene = this.createScene();

    return {engine: this.engine, scene: this.scene};
  }
}
