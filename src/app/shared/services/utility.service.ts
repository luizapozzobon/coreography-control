import { DataTransferService } from './../../app-services/data-transfer.service';
import { SocketInfoService } from './../../app-services/socket-info.service';

import { Injectable } from '@angular/core';

@Injectable()
export class UtilityService {
  message;
  user;

  constructor(
    private socketInfo: SocketInfoService,
    private websocket: DataTransferService) {
      this.socketInfo.destinyChanged.subscribe(value => {
        this.socketInfo.robot_destiny = value;
      });
      this.user = JSON.parse(localStorage.getItem('user'));
     }

  wsUpdateRobots() {
    this.message = {origin: this.user.id, action: 'update_robots'};
    this.websocket.messages.next(this.message);
  }

  wsDisableTorque() {
    if (this.socketInfo.robot_destiny) {
      this.message = {action: 'disable_torque', destiny: this.socketInfo.robot_destiny, data: '', origin: this.user.id};
      this.websocket.messages.next(this.message);
    }
  }

  wsCloseConnection() {
    this.wsDisableTorque();
    this.message = {action: 'close_connection', destiny: this.socketInfo.robot_destiny, data: 'website', origin: this.user.id};
    this.websocket.messages.next(this.message);
    this.socketInfo.robot_destiny = null;
    this.socketInfo.changeDestiny(this.socketInfo.robot_destiny);
  }

}
