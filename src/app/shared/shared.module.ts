
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatTooltipModule, MatCheckboxModule, MatSlideToggleModule } from '@angular/material';

import { UtilityService } from './services/utility.service';
import { BabylonService } from './services/babylon.service';

import { JointSlidersComponent } from './components/joint-sliders/joint-sliders.component';
import { BeoComponent } from './components/beo/beo.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatSlideToggleModule
  ],
  declarations: [
    BeoComponent,
    JointSlidersComponent
  ],
  providers: [
    BabylonService,
    UtilityService
  ],
  exports: [
    BeoComponent,
    JointSlidersComponent
  ]
})
export class SharedModule { }
