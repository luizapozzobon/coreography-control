import { Component, OnInit } from '@angular/core';

import { KeyframesService } from './../../../app-services/keyframes.service';
import { BabylonService } from './../../services/babylon.service';

import * as BABYLON from 'babylonjs';


@Component({
  selector: 'app-beo',
  templateUrl: './beo.component.html',
  styleUrls: ['./beo.component.css']
})
export class BeoComponent implements OnInit {
  keyframes: KeyframesService;
  beoModel: BabylonService;
  loaded = 0;
  previous = 0;

  constructor(beoModel: BabylonService,
              keyframes: KeyframesService) {
    this.keyframes = keyframes;
    this.beoModel = beoModel;
  }

  ngOnInit() {
    this.keyframes.jointsChanged.subscribe();
    this.keyframes.mirrorChanged.subscribe();
    this.renderBeo();
  }

  renderBeo() {
  this.beoModel.onLoad();
  BABYLON.SceneLoader.ImportMesh('', '../../../assets/', 'beo7.babylon', this.beoModel.scene, (newMeshes) => {
    this.loaded = 1;
    // console.log(this.beoModel.scene.meshes); // to print beo's 3d parts
  });

    this.beoModel.engine.runRenderLoop(() => { // loop that renders every motion
      this.beoModel.scene.render();
      if (this.loaded) {
        this.beoModel.scene.meshes[11].rotation.x = -(360*(this.keyframes.jointsMin[0] - this.keyframes.joints[0]) / 1024) * Math.PI/180;
        this.beoModel.scene.meshes[12].rotation.z = (360*(this.keyframes.jointsMin[2] - this.keyframes.joints[2]) / 1024) * Math.PI/180;
        this.beoModel.scene.meshes[13].rotation.x = -(360*(this.keyframes.jointsMin[4] - this.keyframes.joints[4]) / 1024) * Math.PI/180;
        this.beoModel.scene.meshes[15].rotation.z = (360*(- (this.keyframes.jointsMax[6] -
          (this.keyframes.jointsMax[6] - this.keyframes.jointsMin[6]) / 2) + this.keyframes.joints[6]) / 1024) * Math.PI / 180;
        this.beoModel.scene.meshes[15].rotation.x = (360*((this.keyframes.jointsMax[7] -
          (this.keyframes.jointsMax[7] - this.keyframes.jointsMin[7]) / 2) - this.keyframes.joints[7]) / 1024) * Math.PI / 180;
        this.beoModel.scene.meshes[14].rotation.y = (360*((this.keyframes.jointsMax[8] -
          (this.keyframes.jointsMax[8] - this.keyframes.jointsMin[8]) / 2) - this.keyframes.joints[8]) / 1024) * Math.PI / 180;

        if (this.keyframes.mirror) {
          this.keyframes.joints[1] = (this.keyframes.jointsMax[1] + this.keyframes.jointsMin[0] - this.keyframes.joints[0]);
          this.keyframes.joints[3] = (this.keyframes.jointsMax[3] + this.keyframes.jointsMin[2] - this.keyframes.joints[2]);
          this.keyframes.joints[5] = (this.keyframes.jointsMax[5] + this.keyframes.jointsMin[4] - this.keyframes.joints[4]);
          this.beoModel.scene.meshes[8].rotation.x = (360*(this.keyframes.jointsMax[1] - this.keyframes.joints[1]) / 1024) * Math.PI/180;
          this.beoModel.scene.meshes[9].rotation.z = (360*(this.keyframes.jointsMax[3] - this.keyframes.joints[3]) / 1024) * Math.PI/180;
          this.beoModel.scene.meshes[10].rotation.x = (360*(this.keyframes.jointsMax[5] - this.keyframes.joints[5]) / 1024) * Math.PI/180;
        } else {
          this.beoModel.scene.meshes[8].rotation.x = (360*(this.keyframes.jointsMax[1] - this.keyframes.joints[1]) / 1024) * Math.PI/180;
          this.beoModel.scene.meshes[9].rotation.z = (360*(this.keyframes.jointsMax[3] - this.keyframes.joints[3])/ 1024) * Math.PI/180;
          this.beoModel.scene.meshes[10].rotation.x = (360*(this.keyframes.jointsMax[5] - this.keyframes.joints[5]) / 1024) * Math.PI/180;
        }

        this.beoModel.scene.meshes[6].rotation.x = this.keyframes.wheelRotation['left'] || this.keyframes.wheelRotation['both'];
        this.beoModel.scene.meshes[7].rotation.x = this.keyframes.wheelRotation['right'] || this.keyframes.wheelRotation['both'];

        switch (this.keyframes.eyes) {
          case 0:
            this.beoModel.scene.meshes[22].visibility = 1;
            this.beoModel.scene.meshes[21].visibility = this.beoModel.scene.meshes[20].visibility =
              this.beoModel.scene.meshes[19].visibility = 0;
            break;
          case 1:
            this.beoModel.scene.meshes[21].visibility = 1;
            this.beoModel.scene.meshes[22].visibility = this.beoModel.scene.meshes[20].visibility =
             this.beoModel.scene.meshes[19].visibility = 0;
            break;
          case 2:
            this.beoModel.scene.meshes[20].visibility = 1;
            this.beoModel.scene.meshes[22].visibility = this.beoModel.scene.meshes[21].visibility =
             this.beoModel.scene.meshes[19].visibility = 0;
            break;
          case 3:
            this.beoModel.scene.meshes[19].visibility = 1;
            this.beoModel.scene.meshes[22].visibility = this.beoModel.scene.meshes[21].visibility =
             this.beoModel.scene.meshes[20].visibility = 0;
            break;
        }
      }
    });
  }
}
