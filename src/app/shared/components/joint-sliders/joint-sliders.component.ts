import { SocketInfoService } from './../../../app-services/socket-info.service';
import { KeyframesService } from './../../../app-services/keyframes.service';

import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-joint-sliders',
  templateUrl: './joint-sliders.component.html',
  styleUrls: ['./joint-sliders.component.css'],
})
export class JointSlidersComponent implements OnInit {

  keyframes;
  user;
  angles = [];
  message;
  message2;
  @Output() newMessage = new EventEmitter<Object>();

  constructor(
    keyframes: KeyframesService,
    private socketInfo: SocketInfoService
  ) {
    this.keyframes = keyframes;
   }

  ngOnInit() {

    this.socketInfo.destinyChanged.subscribe(value => {
      this.socketInfo.robot_destiny = value;
    });

    this.user = JSON.parse(localStorage.getItem('user'));

    for (let index = 0; index < 9; index++) {
      if (index % 2 === 0 || index > 6) {
        this.angles[index] = Math.round((this.keyframes.joints[index] - this.keyframes.jointsMin[index]) * this.keyframes.angle_proportion);
      } else if (index % 2 !== 0 && index < 6) {
        this.angles[index] = Math.round((this.keyframes.jointsMax[index] - this.keyframes.joints[index]) * this.keyframes.angle_proportion);
      }
    }
  }


  setJoints(index: number) {
    /*
      Sets Robot's (both virtual and real) joints to desired position.
      Also, updates the angle display on screen.
    */
    if (index % 2 === 0 || index > 6) {
      this.angles[index] = Math.round((this.keyframes.joints[index] - this.keyframes.jointsMin[index]) * this.keyframes.angle_proportion);
    } else if (index % 2 !== 0 && index < 6) {
      this.angles[index] = Math.round((this.keyframes.jointsMax[index] - this.keyframes.joints[index]) * this.keyframes.angle_proportion);
    }

    if (this.angles[index] > this.keyframes.angles_max[index]) {
      this.angles[index] = this.keyframes.angles_max[index];
    }

    if (this.keyframes.mirror === true && index % 2 === 0 && index < 6) {
      this.angles[index + 1] = this.angles[index];
    }

    if (this.keyframes.mirror === true && index % 2 === 0 && index < 6) {
      this.message = {destiny: this.socketInfo.robot_destiny, action: 'set_joint', origin: this.user.id,
        data: {value: this.keyframes.joints, index: index}};
      this.message2 = {destiny: this.socketInfo.robot_destiny, action: 'set_joint', origin: this.user.id,
        data: {value: this.keyframes.joints, index: (index + 1)}};
      this.newMessage.emit(this.message);
      this.newMessage.emit(this.message2);
    } else if (this.keyframes.mirror === false) {
      this.message = {destiny: this.socketInfo.robot_destiny, action: 'set_joint', origin: this.user.id,
        data: {value: this.keyframes.joints, index: index}};
      this.newMessage.emit(this.message);
    } else {
      // do nothing;
    }
  }

  ngOnDestroy() {
    this.keyframes.mirror = false;
  }
}
