import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JointSlidersComponent } from './joint-sliders.component';

describe('JointSlidersComponent', () => {
  let component: JointSlidersComponent;
  let fixture: ComponentFixture<JointSlidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JointSlidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointSlidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
