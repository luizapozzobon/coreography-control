import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';
import { FlashMessagesService } from 'angular2-flash-messages';

import { SocketInfoService } from '../../app-services/socket-info.service';
import { KeyframesService } from '../../app-services/keyframes.service';
import { UtilityService } from './../../shared/services/utility.service';
import { DataTransferService } from './../../app-services/data-transfer.service';

import { AuthService } from './../../app-services/auth.service';

import { RobotSocketComponent } from './../../app-components/robot-socket/robot-socket.component';

import * as $ from 'jquery';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  username: String;
  isAdmin: Boolean;
  authService: AuthService;
  message;
  user;
  websocket;
  socketInfo;
  ws;
  keyframes;

  constructor(
    authService: AuthService,
    private router: Router,
    public dialog: MatDialog,
    websocket: DataTransferService,
    socketInfo: SocketInfoService,
    keyframes: KeyframesService,
    private utilityService: UtilityService,
    private flashMessages: FlashMessagesService
    ) {
      this.authService = authService;
      this.websocket = websocket;
      this.socketInfo = socketInfo;
      this.keyframes = keyframes;
   }

  ngOnInit() {
    this.socketInfo.destinyChanged.subscribe(value => {
      this.socketInfo.robot_destiny = value;
    });

    this.user = JSON.parse(localStorage.getItem('user'));
    this.authService.getProfile().subscribe(profile => {
      this.username = profile.user.username;
      this.isAdmin = profile.user.isAdmin;
    },
    err => {
      console.log(err);
      return false;
    });

    this.websocketSubscription();
  }

  websocketSubscription() {
    this.ws = this.websocket.messages.subscribe(msg => {

      if (msg.data.action === 'find_users') {
        this.message = {'origin': this.user.id, 'action': 'register_user'};
        this.onMessage(this.message);
      }

      if (msg.data.action === 'ping') {
        this.message = {'origin': this.user.id, 'action': 'pong', 'destiny': 'server'};
        this.onMessage(this.message);
      }
    });
  }
  onMessage(event) {
    this.websocket.messages.next(event);
  }

  openSocketConfig() {
    const dialogRef = this.dialog.open(RobotSocketComponent, {
      height: '240px'
    });

/*     dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    }); */
  }

  onLogoutClick() {
    this.authService.logout();
    this.flashMessages.show('You are logged out', {cssClass: ['alert-success'], timeout: 3000});
    this.router.navigate(['/login']);
    return false;
  }
}
