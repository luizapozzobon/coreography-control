/*
Pop-up de configuração de conexão dos sites com os robôs
 */

import { Component, OnInit, Output, EventEmitter, OnDestroy, ViewChild } from '@angular/core';

import { UtilityService } from '../../shared/services/utility.service';
import { KeyframesService } from './../../app-services/keyframes.service';
import { DataTransferService } from './../../app-services/data-transfer.service';
import { SocketInfoService } from './../../app-services/socket-info.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-robot-socket',
  templateUrl: './robot-socket.component.html',
  styleUrls: ['./robot-socket.component.css']
})
export class RobotSocketComponent implements OnInit {

  message;
  @Output() newMessage = new EventEmitter<Object>();
    // output para enviar mensagens socket para o componente em que este componente foi 'chamado' (no html)
  user;
  _subscription;
  ws;
  websocket;
  socketInfo;
  utilityService;

  constructor(
    socketInfo: SocketInfoService,
    websocket: DataTransferService,
    private keyframes: KeyframesService,
    utilityService: UtilityService) {
    this.websocket = websocket;
    this.socketInfo = socketInfo;
    this.utilityService = utilityService;
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.socketInfo.destinyChanged.subscribe((value) => {
    });
    this.websocketSubscription();
  }

  websocketSubscription() {
    this.ws = this.websocket.messages.subscribe(msg => {
      /*
      Tratamento de todas as mensagens socket recebidas pelo site
      */

      console.log(msg)

      if (msg.data.action === 'find_users') {        // para registro do site como usuário no servidor socket
        this.message = {'origin': this.user.id, 'action': 'register_user'};
        this.onMessage(this.message);
        //console.log(msg)
      }

      if (msg.data.action === 'ping') {
        this.message = {'origin': this.user.id, 'action': 'pong', 'destiny': 'server'};
        this.onMessage(this.message);
      }

      if (msg.data.action === 'update_robots') {      // para dar update na lista dos robôs conectados
        this.socketInfo.robots = msg.data.data;
        if (this.socketInfo.robots.length === 1) {
          this.wsConnectToRobot(this.socketInfo.robots[0]);
        } else if (this.socketInfo.robots.length === 0) {
          alert('No robots connected.');
        }
        console.log(this.socketInfo.robots)
      }

      if (msg.data.action === 'robot_disconnected') { // quando a conexão com o robô é perdida
        if (msg.data.origin === this.socketInfo.robot_destiny) {
          alert('Connection with robot lost. Robots list refreshed.');
        }
        this.utilityService.wsUpdateRobots();         // atualização automática da lista dos robôs conectados
        this.socketInfo.robot_destiny = null;         // limpar a variável com a identificação do robô conectado ao site
      }

      if (msg.data.action === 'joints_read') {        // adicionar pose do robô (aba 'live' do motion editor)
        const temp = $.extend({}, msg.data.data);
        this.keyframes.tjoints.push(temp);
      }

      if (msg.data.action === 'disconnected') {
        this.utilityService.wsCloseConnection();      // quando um admin conecta ao robô que você estava conectado
        alert('You have been disconnected by an admin.');
      }
      if (msg.data.action === 'not_connected') {      // quando tenta conectar a um robô, mas ele já está sendo utilizado
        this.socketInfo.robot_destiny = null;
        this.utilityService.wsCloseConnection();
        this.socketInfo.changeDestiny(this.socketInfo.robot_destiny);
        alert('Someone is already connected to this robot. Try again later.');
      }
    });
  }

  onMessage(event) {                      // envio das mensagens socket para o componente que usa este componente no HTML
    this.websocket.messages.next(event);
  }

  wsConnectToRobot(robot: any) {          // para conexão com o robô quando clica no nome da lista
    // Match the selected ID with the ID's in array
    if (this.socketInfo.robot_destiny) {
      this.utilityService.wsCloseConnection();
    }
    this.socketInfo.robot_destiny = robot;
    this.socketInfo.changeDestiny(robot);
    this.message = {action: 'connect_to_robot', destiny: this.socketInfo.robot_destiny, origin: this.user.id, data: this.user.isAdmin};
    this.onMessage(this.message);
  }

  ngOnDestroy() {
  }

}
