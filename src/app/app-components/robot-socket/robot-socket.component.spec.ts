import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotSocketComponent } from './robot-socket.component';

describe('RobotSocketComponent', () => {
  let component: RobotSocketComponent;
  let fixture: ComponentFixture<RobotSocketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RobotSocketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotSocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
