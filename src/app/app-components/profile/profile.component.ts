import { SpeechService } from './../../speech/services/speech.service';
import { MotionService } from './../../motion/services/motion.service';

import { Component, OnInit } from '@angular/core';

import * as $ from 'jquery';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user;
  moves = 0;
  speeches = 0;
  motionService;
  speechService;

  constructor(
    motionService: MotionService,
    speechService: SpeechService
  ) {
    this.motionService = motionService;
    this.user = JSON.parse(localStorage.getItem('user'));
    this.speechService = speechService;
  }

  ngOnInit() {
    this.motionService.motionQuantity(this.user.username).subscribe(data => {
      this.moves = data.data;
    });
    this.speechService.speechQuantity(this.user.username).subscribe(data => {
      this.speeches = data.quantity;
    });
  }

  newProfilePic(input) { // função para upload de profile pic, não está finalizada
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = (function (theImage) {
          return function(e) {
            $('.profile-pic').attr('src', e.target.result);
          };
        });
        reader.readAsDataURL(input.files[0]);
    }
  }
}
