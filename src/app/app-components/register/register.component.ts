import { FlashMessagesService } from 'angular2-flash-messages';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ValidateService } from './../../app-services/validate.service';
import { AuthService } from './../../app-services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: String;
  username: String;
  email: String;
  password: String;
  adminCode: String;
  validateService: ValidateService;

  constructor(
    validateService: ValidateService,
    private authService: AuthService,
    private router: Router,
    private flashMessages: FlashMessagesService) {
    this.validateService = validateService;
   }

  ngOnInit() {
  }

  onRegisterSubmit() {
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password,
      adminCode: this.adminCode
    };
    // Validate required fields
    if (!this.validateService.validateRegister(user)) {
      this.flashMessages.show('Please fill in all fields (except admin code, if not admin)', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }
    // Validate email
    if (!this.validateService.validateEmail(user.email)) {
      this.flashMessages.show('Please fill in the email field', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }

    // Register user
    this.authService.registerUser(user).subscribe(data => {
      if (data.success) {
        this.flashMessages.show('You are registered and can login.', {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/login']);
      } else {
        this.flashMessages.show('Something went wrong.', {cssClass: 'alert-danger', timeout: 3000});
        this.router.navigate(['/user']);
      }
    });
  }

}
