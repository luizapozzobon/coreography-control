import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { MatDialogModule, MatFormFieldModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlashMessagesModule } from 'angular2-flash-messages';

import { MotionModule } from './motion/motion.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './app-components/home/home.component';
import { NavbarComponent } from './app-components/navbar/navbar.component';
import { LoginComponent } from './app-components/login/login.component';
import { RegisterComponent } from './app-components/register/register.component';
import { ProfileComponent } from './app-components/profile/profile.component';
import { RobotSocketComponent } from './app-components/robot-socket/robot-socket.component';

import { SocketService } from './app-services/socket.service';
import { DataTransferService } from './app-services/data-transfer.service';
import { ValidateService } from './app-services/validate.service';
import { AuthService } from './app-services/auth.service';
import { KeyframesService } from './app-services/keyframes.service';
import { SocketInfoService } from './app-services/socket-info.service';

import { SpeechService } from './speech/services/speech.service';

import { AuthGuard } from './guards/auth.guard';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'motion', loadChildren: 'app/motion/motion.module#MotionModule' },
  {path: 'speech', loadChildren: 'app/speech/speech.module#SpeechModule' },
  {path: 'scenario', loadChildren: 'app/scenario/scenario.module#ScenarioModule' },
  {path: 'control', loadChildren: 'app/control/control.module#ControlModule'},
  {path: 'buttons', loadChildren: 'app/buttons/buttons.module#ButtonsModule'}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    RobotSocketComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MotionModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserAnimationsModule,
    FlashMessagesModule.forRoot()
  ],
  entryComponents: [RobotSocketComponent],
  providers: [ // serviços são estáticos - sobrescrevem
    SocketService,
    ValidateService,
    AuthService,
    AuthGuard,
    DataTransferService,
    SpeechService,
    SocketInfoService,
    KeyframesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
