import { SpeechInfoService } from './../../services/speech-info.service';
import { SpeechDatabaseComponent } from './../speech-database/speech-database.component';
import { VoiceAdjustmentComponent } from './voice-adjustment/voice-adjustment.component';
import { SpeechService } from './../../services/speech.service';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-create-speech',
  templateUrl: './create-speech.component.html',
  styleUrls: ['./create-speech.component.css']
})
export class CreateSpeechComponent implements OnInit {

  @Output() whensaved = new EventEmitter();

  save;

  synth = window.speechSynthesis;
  text;
  voices = [{lang: 'pt-br', name: 'Português-Brasileiro'}, {lang: 'en', name: 'English'},
    {lang: 'fr', name: 'French'}, {lang: 'de', name: 'German'}, {lang: 'es', name: 'Spanish'}, ];
    voice_option = this.voices[0];
  name;
  user;
  speed;
  pitch;

  constructor(
    private speechService: SpeechService,
    public dialog: MatDialog,
    private flashMessages: FlashMessagesService,
    private speechInfo: SpeechInfoService
  ) {
    speechInfo.pitchValue$.subscribe(data => {
      this.pitch = data;
    });
    speechInfo.speedValue$.subscribe(data => {
      this.speed = data;
    });
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  languageSelection(language_id) {
    this.voice_option = this.voices[language_id];
  }

  voiceAdjust() {
    const dialogRef = this.dialog.open(VoiceAdjustmentComponent, {
      height: '120px',
      width: '450px'
    });

/*     const sub_pitch = dialogRef.componentInstance.onPitchChanged.subscribe((data) => {
      this.pitch = data;
    });
    const sub_speed = dialogRef.componentInstance.onSpeedChanged.subscribe((data) => {
      this.speed = data;
    }); */

    dialogRef.afterClosed().subscribe(result => {
/*       sub_pitch.unsubscribe();
      sub_speed.unsubscribe(); */
      /* console.log(`Dialog result: ${result}`); */
    });
  }

  saveAudio(text) {
    let info;
    if (this.speed === undefined && this.pitch === undefined) {
      info = null;
    } else {
      info = {
        speed: this.speed,
        pitch: this.pitch
      };
    }

    const file_data = {
      text: this.text,
      name: this.name,
      language: this.voice_option,
      creator: this.user.username,
      info: info
    };

    this.speechService.generateTTS(file_data).subscribe(data => {
      if (data.result === 'finished') {
        this.flashMessages.show('Speech saved.', {cssClass: 'alert-success', timeout: 3000});
        this.whensaved.emit(data.success);
      }
    });
  }
}
