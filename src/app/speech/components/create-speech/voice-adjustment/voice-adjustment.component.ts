import { CreateSpeechComponent } from './../create-speech.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { SpeechInfoService } from '../../../services/speech-info.service';

@Component({
  selector: 'app-voice-adjustment',
  templateUrl: './voice-adjustment.component.html',
  styleUrls: ['./voice-adjustment.component.css'],

})
export class VoiceAdjustmentComponent implements OnInit {

  pitch = this.speechInfo.pitch;
  speed = this.speechInfo.speed;
/*   onPitchChanged = new EventEmitter();
  onSpeedChanged = new EventEmitter(); */

  constructor(private speechInfo: SpeechInfoService) {
      speechInfo.pitchValue$.subscribe(data => {
        this.pitch = data;
      });
      speechInfo.speedValue$.subscribe(data => {
        this.speed = data;
      });
   }

  ngOnInit() {
  }

  onPitchChange() {
    this.speechInfo.changePitch(this.pitch);
    // this.onPitchChanged.emit(this.pitch);
  }

  onSpeedChange() {
    this.speechInfo.changeSpeed(this.speed);
    // this.onSpeedChanged.emit(this.speed);
  }


}
