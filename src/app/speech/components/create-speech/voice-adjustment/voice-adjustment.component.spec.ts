import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoiceAdjustmentComponent } from './voice-adjustment.component';

describe('VoiceAdjustmentComponent', () => {
  let component: VoiceAdjustmentComponent;
  let fixture: ComponentFixture<VoiceAdjustmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoiceAdjustmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoiceAdjustmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
