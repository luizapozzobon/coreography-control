import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeechDatabaseComponent } from './speech-database.component';

describe('SpeechDatabaseComponent', () => {
  let component: SpeechDatabaseComponent;
  let fixture: ComponentFixture<SpeechDatabaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeechDatabaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeechDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
