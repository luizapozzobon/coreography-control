import { SocketInfoService } from './../../../app-services/socket-info.service';
import { DataTransferService } from './../../../app-services/data-transfer.service';
import { CreateSpeechComponent } from './../create-speech/create-speech.component';
import { PlaySpeechService } from './../../services/play-speech.service';
import { SpeechService } from './../../services/speech.service';
import { MatTableDataSource } from '@angular/material';

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-speech-database',
  templateUrl: './speech-database2.component.html',
  styleUrls: ['./speech-database.component.css']
})
export class SpeechDatabaseComponent implements OnInit {

  list;
  websocket;
  socketInfo;
  user;
  r;
  displayedColumns = ['options', 'name', 'language', 'creator'];

  constructor(
    private speechService: SpeechService,
    private playSpeechService: PlaySpeechService,
    websocket: DataTransferService,
    socketInfo: SocketInfoService
  ) {
    this.socketInfo = socketInfo;
    this.websocket = websocket;
   }

  ngOnInit() {
    this.updateList('nada');
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  updateList(event) {
    this.speechService.getSpeechList().subscribe(data => {
      this.list = new MatTableDataSource(data.list);
    });
  }

  trackSpeeches(index, file) {
    return file ? file.name : undefined;
  }

  playSpeech(i) {
    let robot_play = true;
    this.playSpeechService.playSpeech(this.list.data[i]._id, robot_play);
  }

  deleteSpeech(i) {
    this.speechService.deleteSpeech(this.list.data[i]._id).subscribe(data => {
      this.updateList('update');
    });
  }

  generateDownloadUri(i) {
    console.log(i);
    this.speechService.playSpeech(this.list.data[i]._id).subscribe(data => {
      let speech = this.playSpeechService.toArrayBuffer(data.data);
      console.log(data, this.list.data)
      let download_file = speech;
      let a = document.createElement('a');
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      let url = window.URL.createObjectURL(new Blob([download_file]));
      a.href = url;
      a.download = this.list.data[i].filename + '.mp3';
      a.click();

      let message = {action: 'download_speech', origin: this.user.id, destiny: this.socketInfo.robot_destiny,
        data: {filename: this.list.data[i].filename, data: data}};
      this.websocket.messages.next(message);
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.list.filter = filterValue;
  }

  changedTab(tab) {
    if (tab === 1) {
      this.list.filter = 'Qiron';
    } else if (tab === 2) {
      this.list.filter = this.user.username;
    } else {
      this.list.filter = '';
    }
  }
}
