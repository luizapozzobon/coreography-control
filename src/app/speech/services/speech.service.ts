import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { AuthService } from './../../app-services/auth.service';

@Injectable()
export class SpeechService {

  constructor(
      private http: Http,
      private authService: AuthService
    ) { }

  generateTTS(data) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'speeches/generate', data, {headers: headers})
      .map(res => res.json());
  }

  liveSpeech(data) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'speeches/live/',
     {headers: headers, params: {text: data.text, lang: data.language}})
     .map(res => res.json());
  }

  deleteSpeech(speechID) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.delete(this.authService.ip + 'speeches/' + speechID, {headers: headers})
    .map(res => res.json());
  }

  getSpeechList() {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'speeches', {headers: headers})
    .map(res => res.json());
  }

  playSpeech(speechID) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-type', 'audio/mp3');
    return this.http.get(this.authService.ip + 'speeches/' + speechID + '/play', {headers: headers})
    .map(res => res.json());
  }

  adjustSpeech(info) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'speeches/adjust', info, {headers: headers})
      .map(res => res.json());
  }

  speechQuantity(username) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'speeches/' + username + '/quantity', {headers: headers})
      .map(res => res.json());
  }

}
