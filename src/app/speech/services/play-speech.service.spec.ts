import { TestBed, inject } from '@angular/core/testing';

import { PlaySpeechService } from './play-speech.service';

describe('PlaySpechService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlaySpeechService]
    });
  });

  it('should be created', inject([PlaySpeechService], (service: PlaySpechService) => {
    expect(service).toBeTruthy();
  }));
});
