import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class SpeechInfoService {

  speed = 1.1;
  pitch = 110;

  pitchValue = new Subject<number>();
  speedValue = new Subject<number>();

  pitchValue$ = this.pitchValue.asObservable();
  speedValue$ = this.speedValue.asObservable();

  constructor() { }

  changePitch(new_pitch) {
    this.pitchValue.next(new_pitch);
    this.pitch = new_pitch;
  }

  changeSpeed(new_speed) {
    this.speedValue.next(new_speed);
    this.speed = new_speed;
  }
}
