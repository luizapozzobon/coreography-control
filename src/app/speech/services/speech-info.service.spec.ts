import { TestBed, inject } from '@angular/core/testing';

import { SpeechInfoService } from './speech-info.service';

describe('SpeechInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpeechInfoService]
    });
  });

  it('should be created', inject([SpeechInfoService], (service: SpeechInfoService) => {
    expect(service).toBeTruthy();
  }));
});
