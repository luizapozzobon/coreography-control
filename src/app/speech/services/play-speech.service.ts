import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';

import { SocketInfoService } from '../../app-services/socket-info.service';
import { DataTransferService } from './../../app-services/data-transfer.service';
import { SpeechService } from './speech.service';

@Injectable()
export class PlaySpeechService {

  buffer;
  context = new AudioContext();
  list = new Array();
  speechService;
  user;
  ws;

  constructor(
    speechService: SpeechService,
    private socketInfo: SocketInfoService,
    websocket: DataTransferService
  ) {
    this.speechService = speechService;
    this.ws = websocket;
    this.user = JSON.parse(localStorage.getItem('user'));
   }

  toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
  }

  playSound() {
    let source = this.context.createBufferSource();
    source.buffer = this.buffer;
    source.connect(this.context.destination);
    source.start(0);
    console.log('foi')
  }

  prepareToPlay(data, robot_play) {
    let speech = this.toArrayBuffer(data);
    var self = this;
    if (robot_play) {
      let message = {destiny: this.socketInfo.robot_destiny, action: 'play_speech', data: data, origin: this.user.id};
      this.ws.messages.next(message);
      console.log(robot_play, message)
    }
    this.context.decodeAudioData(speech, function(buffer) {
      self.buffer = buffer;
      self.playSound();
    });
  }

  playSpeech(id, robot_play) {
    this.speechService.playSpeech(id).subscribe(data => {
      this.prepareToPlay(data.data, robot_play);
    });
  }

}
