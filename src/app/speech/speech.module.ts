import { FlashMessagesModule } from 'angular2-flash-messages';
import { SpeechInfoService } from './services/speech-info.service';
import { MatSelectModule, MatTableModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatTabsModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { SocketInfoService } from './../app-services/socket-info.service';
import { PlaySpeechService } from './services/play-speech.service';
import { SpeechService } from './services/speech.service';

import { CreateSpeechComponent } from './components/create-speech/create-speech.component';
import { SpeechDatabaseComponent } from './components/speech-database/speech-database.component';

import { AuthGuard } from './../guards/auth.guard';
import { VoiceAdjustmentComponent } from './components/create-speech/voice-adjustment/voice-adjustment.component';

const speechRoutes: Routes = [
  {path: '', component: SpeechDatabaseComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatTabsModule,
    RouterModule.forChild(speechRoutes),
    FlashMessagesModule.forRoot()
  ],
  entryComponents: [VoiceAdjustmentComponent],
  declarations: [
    SpeechDatabaseComponent,
    CreateSpeechComponent,
    VoiceAdjustmentComponent
  ],
  providers: [
    PlaySpeechService,
    SpeechService,
    SpeechInfoService
  ],
  exports: [
  ]
})
export class SpeechModule { }
