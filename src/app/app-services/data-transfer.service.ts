/*
  Serviço de configuração de conexão ao servidor socket e que controla o recebimento/envio das mensagens.
  Chamado no serviço/componente em que for necessário enviar/receber mensagens.
 */

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { SocketService } from './socket.service';

//const server_url = 'ws://localhost:15315/';      // local development ip
const server_url = 'ws://207.38.86.37:15315/';      // webfaction socket server ip

export interface Message {
  data;
}

@Injectable()
export class DataTransferService {
  public messages: Subject<Message>;

  constructor(wsService: SocketService) {
    this.messages = <Subject<Message>>wsService
      .connect(server_url)
      .map((response: MessageEvent): Message => {
        let data2 = JSON.parse(response.data);
        return {
          data: data2
        };
      });
  }
}
