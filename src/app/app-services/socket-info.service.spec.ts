import { TestBed, inject } from '@angular/core/testing';

import { SocketInfoService } from './socket-info.service';

describe('SocketInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocketInfoService]
    });
  });

  it('should be created', inject([SocketInfoService], (service: SocketInfoService) => {
    expect(service).toBeTruthy();
  }));
});
