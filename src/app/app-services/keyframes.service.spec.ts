import { TestBed, inject } from '@angular/core/testing';

import { KeyframesService } from './keyframes.service';

describe('KeyframesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KeyframesService]
    });
  });

  it('should be created', inject([KeyframesService], (service: KeyframesService) => {
    expect(service).toBeTruthy();
  }));
});
