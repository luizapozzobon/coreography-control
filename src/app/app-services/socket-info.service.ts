/*
  Serviço das informações relacionadas à conexão socket (site - robô)
*/

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SocketInfoService {

  robots = new Array();
  robot_destiny: any;       // robô conectado atualmente ao site
  public destinyChanged: Subject<String> = new Subject();

  constructor() { }

  changeDestiny(destiny) {
    this.destinyChanged.next(destiny);
  }
}
