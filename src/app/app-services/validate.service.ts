/*
  Serviço de validação dos campos de login, registro e durações (no motion editor)
 */

import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateRegister(user) {
    if (user.name == undefined || user.email == undefined || user.username == undefined || user.password == undefined) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateDurations(keyframes_table) {
    /*
      O campo de durações no motion editor necessita ser preenchido com números maiores
      ou iguais a 0 para retornar true.
    */

    let cont = 0;
    let cont_neg = 0
    for (let i in keyframes_table) {
      if (keyframes_table[i][9] !== undefined && keyframes_table[i][9] !== null) {
        cont++;
      }
      if (keyframes_table[i][9] >= 0) {
        cont_neg++
      }
    }
    if (cont === keyframes_table.length && cont_neg === keyframes_table.length) {
      return true;
    } else {
      return false;
    }
  }

  validateName(name) {
    if (name === null || name === undefined || name === '') {
      return false;
    } else {
      return true;
    }
  }

}
