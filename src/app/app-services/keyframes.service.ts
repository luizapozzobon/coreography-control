/*
  Serviço com as informações e dados relacionados à movimentação das juntas do Beo.
 */

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class KeyframesService {
  joints: any;
  default_joints;
  jointsMin: any;
  jointsMax: any;
  jointsCenter: any;
  tjoints: any;
  angles_max: any;
  angle_proportion: any;
  mirror: any;
  wheelRotation;
  eyes;
  jointsChanged = new Subject<void>();
  mirrorChanged = new Subject<void>();
  tjointsChanged = new Subject<void>();

  constructor() {

    // SE ALTERAR VALORES DO CÓDIGO DO BEO, ALTERAR AQUI TAMBÉM

    this.joints    = { 0: 251, 1: 771, 2: 559, 3: 476, 4: 585, 5: 754, 6: 500, 7: 567, 8: 512 };
    this.default_joints = { 0: 251, 1: 771, 2: 559, 3: 476, 4: 585, 5: 754, 6: 500, 7: 567, 8: 512 }; // 6 was 450
    this.jointsMin = { 0: 203, 1: 203, 2: 500, 3: 223, 4: 520, 5: 520, 6: 400, 7: 470, 8: 370 }; // 6 was 350
    this.jointsMax = { 0: 820, 1: 820, 2: 808, 3: 531, 4: 828, 5: 828, 6: 600, 7: 650, 8: 650 }; // 6 was 550
    this.jointsCenter = { 0: 203, 1: 820, 2: 500, 3: 531, 4: 520, 5: 820, 6: 450, 7: 560, 8: 510 };
    this.angles_max = { 0: 180, 1: 180, 2: 90, 3: 90, 4: 90, 5: 90, 6: 57, 7: 52, 8: 81 };
    this.angle_proportion = 0.29296875;
    this.tjoints   = [];
    this.mirror    = false;

    this.wheelRotation = {'left': 0, 'right': 0, 'both': 0};
  }

  // Essas funções seriam para fazer com que qualquer mudança em qualquer lugar do site nas variáveis
  // joints, tjoints e mirror fossem aplicadas também em todos os componentes e serviços do site.
  // Porém, talvez não sejam mais necessárias.

  onJointsChange() {
    this.jointsChanged.next(this.joints);
  }

  onTJointsChange(tjoints) {
    this.tjointsChanged.next(tjoints);
  }

  onMirrorChange() {
    this.mirrorChanged.next(this.mirror);
  }


}
