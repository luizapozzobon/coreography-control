/*
  Solicitações do front para o back-end, dos componentes e serviços gerais
  Configuração do IP do back-end
 */

import { tokenNotExpired } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  authToken: any;
  user: any;
  //ip = 'http://localhost:21744/'; // computer back-end server ip
  ip = ''; // webfaction

  constructor(private http: Http) { }

  registerUser(user) {
    let headers = new Headers;
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.ip + 'users/register', user, {headers: headers})
      .map(res => res.json()); // observable
  }

  authenticateUser(user) {
    let headers = new Headers;
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.ip + 'users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  getProfile() {
    let headers = new Headers;
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.ip + 'users/profile', {headers: headers})
      .map(res => res.json());
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    // because localstorage only stores strings, not objects
    this.authToken = token;
    this.user = user;
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }
}
