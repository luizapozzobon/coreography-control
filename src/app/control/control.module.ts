import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from './../shared/shared.module';

import { MatListModule, MatTooltipModule } from '@angular/material';

import { PlayScenarioService } from './../scenario/services/play-scenario.service';
import { SocketInfoService } from './../app-services/socket-info.service';
import { PlayMotionService } from './../motion/services/play-motion.service';
import { PlaySpeechService } from './../speech/services/play-speech.service';
import { ScenarioService } from '../scenario/services/scenario.service';
import { UtilityService } from '../shared/services/utility.service';

import { ControlComponent } from './components/control/control.component';
import { LiveWheelsComponent } from './components/live-wheels/live-wheels.component';
import { ControlListComponent } from './components/control-list/control-list.component';
import { LiveSpeechComponent } from './components/live-speech/live-speech.component';

import { AuthGuard } from './../guards/auth.guard';

const controlRoutes: Routes = [
  {path: '', component: ControlComponent, canActivate: [AuthGuard]}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(controlRoutes),
    SharedModule,
    FormsModule,
    MatListModule,
    MatTooltipModule
  ],
  declarations: [
    ControlComponent,
    LiveWheelsComponent,
    ControlListComponent,
    LiveSpeechComponent
  ],
  providers: [
    PlaySpeechService,
    PlayMotionService,
    ScenarioService,
    PlayScenarioService
  ]
})
export class ControlModule { }
