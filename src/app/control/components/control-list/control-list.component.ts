import { MatTableDataSource } from '@angular/material';
import { PlayScenarioService } from './../../../scenario/services/play-scenario.service';
import { KeyframesService } from './../../../app-services/keyframes.service';
import { SocketInfoService } from './../../../app-services/socket-info.service';
import { PlayMotionService } from './../../../motion/services/play-motion.service';
import { PlaySpeechService } from './../../../speech/services/play-speech.service';
import { ScenarioService } from './../../../scenario/services/scenario.service';
import { SpeechService } from './../../../speech/services/speech.service';
import { MotionService } from './../../../motion/services/motion.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-control-list',
  templateUrl: './control-list.component.html',
  styleUrls: ['./control-list.component.css']
})
export class ControlListComponent implements OnInit {
  motionList;
  speechList;
  scenarioList;
  user;
  socketInfo;
  keyframes;
  message;
  eyesOptions = [{option: 'Default', id: 0}, {option: 'Mad', id: 1}, {option: 'Sad', id: 2},
  {option: 'Wink', id: 3}, {option: 'Shut', id: 4, type: 'eye'}];
  @Output() newMessage = new EventEmitter<Object>();

  constructor(
    private motionService: MotionService,
    private speechService: SpeechService,
    private scenarioService: ScenarioService,
    private playSpeech: PlaySpeechService,
    private playMotion: PlayMotionService,
    private playScenarioService: PlayScenarioService,
    socketInfo: SocketInfoService,
    keyframes: KeyframesService
  ) {
    this.socketInfo = socketInfo;
    this.keyframes = keyframes;
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));

    this.speechService.getSpeechList().subscribe(data => {
      this.speechList = data.list;
    });
    this.motionService.getMotionsList().subscribe(data => {
      this.motionList = data;
    });
    this.scenarioService.getScenariosList().subscribe(data => {
      this.scenarioList = data;
    });
  }

/*
  Funções para aquisição dos dados de movimentos, falas e cenários quando clicados na tabela
  e sua posterior execução.
 */

  tablePlayMotion(motion) {
    this.motionService.getMotion(motion.name).subscribe(data => {
      this.keyframes.tjoints = data.data.poses;
      this.playMotion.playMotion(data.data.poses);
    });
  }

  tablePlaySpeech(speech) {
    let robot_play = true;
    this.playSpeech.playSpeech(speech._id, robot_play);
  }

  tablePlayScenario(scenario) {
    this.scenarioService.getScenario(scenario.name).subscribe(data => {
      this.playScenarioService.playScenarioEach(data.scenario.items);
    });
  }

  setEyes(option) {
    this.keyframes.eyes = option.id;
    this.message = {data: option, destiny: this.socketInfo.robot_destiny, action: 'set_expression', origin: this.user.id};
    this.newMessage.emit(this.message);
  }
}
