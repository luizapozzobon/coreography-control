import { RobotSocketComponent } from './../../../app-components/robot-socket/robot-socket.component';
import { UtilityService } from './../../../shared/services/utility.service';
import { SocketInfoService } from './../../../app-services/socket-info.service';
import { DataTransferService } from './../../../app-services/data-transfer.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {

  websocket;
  ws;
  message;
  user;

  constructor(
    websocket: DataTransferService,
    private socketInfo: SocketInfoService,
    private utilityService: UtilityService
  ) {
    this.websocket = websocket;
  }

  ngOnInit() {
    this.socketInfo.destinyChanged.subscribe((value) => {
      this.socketInfo.robot_destiny = value;
    });
  }

  onMessage(event) {
    // Envio das mensagens socket recebidas por componentes chamados no html deste

    this.message = event;
    this.websocket.messages.next(this.message);
  }

  ngOnDestroy() {
    this.utilityService.wsCloseConnection();
  }
}
