import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveWheelsComponent } from './live-wheels.component';

describe('LiveWheelsComponent', () => {
  let component: LiveWheelsComponent;
  let fixture: ComponentFixture<LiveWheelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveWheelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveWheelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
