import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HostListener } from '@angular/core';

import { SocketInfoService } from './../../../app-services/socket-info.service';
import { DataTransferService } from './../../../app-services/data-transfer.service';

@Component({
  selector: 'app-live-wheels',
  templateUrl: './live-wheels.component.html',
  styleUrls: ['./live-wheels.component.css']
})
export class LiveWheelsComponent implements OnInit {
  socketInfo;
  key = '';
  keys = {'ArrowUp': 0, 'ArrowDown': 0, 'ArrowLeft': 0, 'ArrowRight': 0, }
    // para controle de qual tecla do teclado esta sendo clicada
  message;
  allowed = false;
  @Output() newMessage = new EventEmitter<Object>();
    // output para envio de mensagens socket através do componente em que este foi chamado (html)
  user;

  constructor(
    socketInfo: SocketInfoService
  ) {
    this.socketInfo = socketInfo;
   }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardDown(event: KeyboardEvent) {
    /*
      Quando um determinado conjunto de setas é clicado,
      é enviada uma mensagem socket com a direção de rotação para o robô
    */
    if (!event.repeat) {
      this.key = event.key;
      this.keys[this.key] = 1;
      let direction;
      if (this.keys['ArrowUp'] === 1 && this.keys['ArrowLeft'] === 1) {
        direction = 'forward_left';
      } else if (this.keys['ArrowUp'] === 1 && this.keys['ArrowRight'] === 1) {
        direction = 'forward_right';
      } else if (this.keys['ArrowDown'] === 1 && this.keys['ArrowLeft'] === 1) {
        direction = 'backward_left';
      } else if (this.keys['ArrowDown'] === 1 && this.keys['ArrowRight'] === 1) {
        direction = 'backward_right';
      } else if (this.keys['ArrowUp'] === 1) {
        direction = 'forwards';
      } else if (this.keys['ArrowDown'] === 1) {
        direction = 'backwards';
      } else if (this.keys['ArrowLeft'] === 1) {
        direction = 'left';
      } else if (this.keys['ArrowRight'] === 1) {
        direction = 'right';
      }
      if (direction) {
        this.message = {action: 'move_wheels', data: direction, origin: this.user.id, destiny: this.socketInfo.robot_destiny}
        this.newMessage.emit(this.message);
      }
    }
  }


  @HostListener('document:keyup', ['$event'])
  handleKeyboardUp(event: KeyboardEvent) {
    /*
      Quando um determinado conjunto de setas é 'desclicado',
      é enviada uma mensagem socket com para o robô parar de girar as rodas.
    */
    this.key = event.key;
    this.keys[this.key] = 0;
    let direction;
    if (this.keys['ArrowUp'] === 0 && this.keys['ArrowLeft'] === 0) {
      direction = 'forward_left';
    } else if (this.keys['ArrowUp'] === 0 && this.keys['ArrowRight'] === 0) {
      direction = 'forward_right';
    } else if (this.keys['ArrowDown'] === 0 && this.keys['ArrowLeft'] === 0) {
      direction = 'backward_left';
    } else if (this.keys['ArrowDown'] === 0 && this.keys['ArrowRight'] === 0) {
      direction = 'backward_right';
    } else if (this.keys['ArrowUp'] === 0) {
      direction = 'forwards';
    } else if (this.keys['ArrowDown'] === 0) {
      direction = 'backwards';
    } else if (this.keys['ArrowLeft'] === 0) {
      direction = 'left';
    } else if (this.keys['ArrowRight'] === 0) {
      direction = 'right';
    }
    if (direction) {
      this.message = {action: 'stop_wheels', data: direction, origin: this.user.id, destiny: this.socketInfo.robot_destiny};
      this.newMessage.emit(this.message);
    }
  }

}
