import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveSpeechComponent } from './live-speech.component';

describe('LiveSpeechComponent', () => {
  let component: LiveSpeechComponent;
  let fixture: ComponentFixture<LiveSpeechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveSpeechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveSpeechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
