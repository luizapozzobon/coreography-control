import { PlaySpeechService } from './../../../speech/services/play-speech.service';
import { Component, OnInit } from '@angular/core';
import { SpeechService } from '../../../speech/services/speech.service';

@Component({
  selector: 'app-live-speech',
  templateUrl: './live-speech.component.html',
  styleUrls: ['./live-speech.component.css']
})
export class LiveSpeechComponent implements OnInit {
  speech;
  user;
  voices = [{lang: 'en', name: 'English'}, {lang: 'pt-br', name: 'Português-Brasileiro'}];
    // linguagem das vozes para seleção
  voice_option = this.voices[1]; // voz escolhida como padrão (pt-br)

  constructor(
    private speechService: SpeechService,
    private playSpeech: PlaySpeechService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  liveSpeech() {
    let file_data = {
      text: this.speech,
      name: 'temp_speech',
      language: 'pt-br',
      creator: this.user.username
    };
    console.log(this.speech)
    let robot_play = true;
    this.speechService.liveSpeech(file_data).subscribe(data => {
      this.playSpeech.playSpeech(data.data._id, robot_play);
    });
  }

  languageSelection(language_id) {
    /*
      Para seleção da linguagem da voz
      Não implementado no html
    */

    this.voice_option = this.voices[language_id];
  }
}
