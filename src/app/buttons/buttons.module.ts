import { PlaySpeechService } from './../speech/services/play-speech.service';
import { PlayScenarioService } from './../scenario/services/play-scenario.service';
import { AuthGuard } from './../guards/auth.guard';
import { ScenarioService } from './../scenario/services/scenario.service';
import { MatSelectModule, MatFormFieldModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetButtonsComponent } from './components/set-buttons/set-buttons.component';
import { Routes, RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const buttonsRoutes: Routes = [
  {path: '', component: SetButtonsComponent, canActivate: [AuthGuard]}
];


@NgModule({
  imports: [
    CommonModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    RouterModule.forChild(buttonsRoutes),
  ],
  providers: [
    ScenarioService,
    PlayScenarioService,
    PlaySpeechService
  ],
  declarations: [SetButtonsComponent]
})
export class ButtonsModule { }
