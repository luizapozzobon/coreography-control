import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetButtonsComponent } from './set-buttons.component';

describe('SetButtonsComponent', () => {
  let component: SetButtonsComponent;
  let fixture: ComponentFixture<SetButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
