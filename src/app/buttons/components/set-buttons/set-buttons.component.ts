import { SocketInfoService } from './../../../app-services/socket-info.service';
import { DataTransferService } from './../../../app-services/data-transfer.service';
import { ScenarioService } from './../../../scenario/services/scenario.service';
import { Component, OnInit } from '@angular/core';
import { PlayScenarioService } from '../../../scenario/services/play-scenario.service';

@Component({
  selector: 'app-set-buttons',
  templateUrl: './set-buttons.component.html',
  styleUrls: ['./set-buttons.component.css']
})
export class SetButtonsComponent implements OnInit {

  user;
  list;
  top;
  mid;
  bottom;
  touch;

  constructor(
    private scenarioService: ScenarioService,
    private playScenario: PlayScenarioService,
    private ws: DataTransferService,
    private socketInfo: SocketInfoService,
  ) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));

    this.scenarioService.getScenariosList().subscribe(data => {
      this.list = data;
    });
  }

  setButton(button, scen_name) {
    /*
      Para atribuir arquivo de cenário a um único botão.
    */
    this.scenarioService.getScenario(scen_name).subscribe(data => {
      this.playScenario.getData(data.scenario.items).then(data2 => {
        let message = {data: {button: button, data: data2}, action: 'set_button', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny};
        this.ws.messages.next(message);
      });
    });
  }

  setAll() {
    /*
      Para atribuir arquivo de cenário a todos os botões.
      Primeiro os itens do cenário selecionado são requisitados,
      e a seguir os dados de cada item são requisitos ao back-end.
    */
    this.scenarioService.getScenario(this.top).subscribe(data => {
      this.playScenario.getData(data.scenario.items).then(data2 => {
        let message = {data: {button: 'top', data: data2}, action: 'set_button', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny};
        this.ws.messages.next(message);
      });
    });

    this.scenarioService.getScenario(this.mid).subscribe(data => {
      this.playScenario.getData(data.scenario.items).then(data2 => {
        let message = {data: {button: 'mid', data: data2}, action: 'set_button', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny};
        this.ws.messages.next(message);
      });
    });

    this.scenarioService.getScenario(this.bottom).subscribe(data => {
      this.playScenario.getData(data.scenario.items).then(data2 => {
        let message = {data: {button: 'bottom', data: data2}, action: 'set_button', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny};
        this.ws.messages.next(message);
      });
    });

    this.scenarioService.getScenario(this.touch).subscribe(data => {
      this.playScenario.getData(data.scenario.items).then(data2 => {
        let message = {data: {button: 'touch', data: data2}, action: 'set_button', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny};
        this.ws.messages.next(message);
      });
    });
  }

}
