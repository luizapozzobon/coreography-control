import { SocketInfoService } from './../../../app-services/socket-info.service';
import { DataTransferService } from './../../../app-services/data-transfer.service';
import { Component, OnInit } from '@angular/core';

import { MotionService } from './../../services/motion.service';
import { MatTableDataSource } from '@angular/material';

import * as $ from 'jquery';

@Component({
  selector: 'app-motion-database',
  templateUrl: './motion-database.component.html',
  styleUrls: ['./motion-database.component.css']
})
export class MotionDatabaseComponent implements OnInit {
  list;
  username: any;
  isAdmin: Boolean;
  search: String = '';
  new_list;
  download_file: any = [];
  user;
  displayedColumns = ['options', 'name', 'creator'];
  websocket;
  socketInfo;

  constructor (
    private motionService: MotionService,
    socketInfo: SocketInfoService,
    websocket: DataTransferService
  ) {
    this.socketInfo = socketInfo;
    this.websocket = websocket;
  }

  ngOnInit() {
    this.motionService.getMotionsList().subscribe(data => {
      this.list = new MatTableDataSource(data);  // necessário este tipo de variável para aplicação do filtro
    },
    err => {
      console.log(err);
      return false;
    });

    this.user = JSON.parse(localStorage.getItem('user'));
  }

  applyFilter(filterValue: string) {
    /*
      Aplicação do filtro de busca da tabela do banco de dados de movimentos
    */
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.list.filter = filterValue;
  }

  changedTab(tab) {
    if (tab === 1) { // quando estiver na aba "Basic Motions", só aparecerão movimentos criados pelo usuário Qiron
      this.list.filter = 'Qiron';
    } else if (tab === 2) { // aba "Your Motions", movimentos criados pelo usuário atual
      this.list.filter = this.user.username;
    } else {
      this.list.filter = '';
    }
  }

  deleteMotion(file: any) {
    const authorization = {
      motion_creator: file.creator,
      current_user: this.user.username,
      isAdmin: this.user.isAdmin
    };

    // Verificação de autorização para deleção de arquivos de movimento
    // Autorizados: o próprio usuário ou admins
    if (authorization.motion_creator === authorization.current_user || authorization.isAdmin === true) {
      this.motionService.removeMotion(file.name).subscribe(data => {
        this.motionService.getMotionsList().subscribe(data2 => {
          this.list = new MatTableDataSource(data2);
        });
      });
    } else {
      alert('Only the creator or an admin can remove a motion file.');
    }
  }

/*   onSearchChange() {
    this.motionService.getMotionSearch({search: this.search}).subscribe(data => {
      this.new_list = data.data;
    });
  }

  trackMotions(index, file) {
    return file ? file.name : undefined;
  }
 */
  generateDownloadUri(motion) {
    /*
      Download dos arquivos de movimento para o computador do usuário.
      Se estiver conectado a um robô, o movimento será baixado automaticamente no mesmo.
    */
    this.motionService.downloadMotion(motion).subscribe( (data) => {
      let download_file = data.data;
      let a = document.createElement('a');
      a.setAttribute('style', 'display:none;');
      document.body.appendChild(a);
      let url = window.URL.createObjectURL(new Blob([download_file]));
      a.href = url;
      a.download = motion + '.beo';
      a.click();

      let message = {action: 'download_motion', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny, data: {filename: motion, poses: data}};
      this.websocket.messages.next(message);
    });
  }


  onFileUpload(event) {
    /*
      Para upload de arquivos de movimento - precisam ser pickle
    */
    let f = event.srcElement.files[0];
    const reader = new FileReader();

    let name = f.name.replace(/\.[^/.]+$/, '');

    let self = this; // para 'this' funcionar dentro do reader.onload

    reader.onload = (function(theFile) {
      return function(e) {
        let file = {name: name, poses: e.target.result, creator: self.user.username};
        self.motionService.uploadMotion(file).subscribe(data1 => {
          if (data1.success) {
            self.motionService.getMotionsList().subscribe(data2 => {
              self.list = data2;
            });
          }
        });
      };
    })(f);
    reader.readAsText(f);
  }

}
