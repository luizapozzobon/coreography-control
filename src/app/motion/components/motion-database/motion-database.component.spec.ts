import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MotionDatabaseComponent } from './motion-database.component';

describe('MotionDatabaseComponent', () => {
  let component: MotionDatabaseComponent;
  let fixture: ComponentFixture<MotionDatabaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotionDatabaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MotionDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
