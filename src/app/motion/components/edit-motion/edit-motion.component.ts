import { Component, OnInit, ViewChild } from '@angular/core';

import { UtilityService } from './../../../shared/services/utility.service';
import { SocketInfoService } from './../../../app-services/socket-info.service';
import { DataTransferService } from './../../../app-services/data-transfer.service';
import { KeyframesService } from './../../../app-services/keyframes.service';

import { RobotSocketComponent } from './../../../app-components/robot-socket/robot-socket.component';
import { InputPosesComponent } from './input-poses/input-poses.component';
import { PosesTableComponent } from './poses-table/poses-table.component';

import * as $ from 'jquery';

@Component({
  selector: 'app-edit-motion',
  templateUrl: './edit-motion.component.html',
  styleUrls: ['./edit-motion.component.css']
})

export class EditMotionComponent implements OnInit {

  keyframes;
  user;
  websocket;
  @ViewChild(RobotSocketComponent)
  robotSocket: RobotSocketComponent;

  constructor(
    websocket: DataTransferService,
    keyframes: KeyframesService,
    private utilityService: UtilityService) {
    this.keyframes = keyframes;
    this.websocket = websocket;
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }


  onMessage(event) {
    this.websocket.messages.next(event);
  }

  ngOnDestroy() {
    this.utilityService.wsCloseConnection();
    let temp = JSON.parse(JSON.stringify($.extend({}, this.keyframes.default_joints)));
    this.keyframes.joints = temp;
    this.keyframes.tjoints = [];
  }
}
