import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputPosesComponent } from './input-poses.component';

describe('InputPosesComponent', () => {
  let component: InputPosesComponent;
  let fixture: ComponentFixture<InputPosesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputPosesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputPosesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
