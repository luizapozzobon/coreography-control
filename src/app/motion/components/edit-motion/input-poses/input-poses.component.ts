import { Component, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';

import { UtilityService } from './../../../../shared/services/utility.service';
import { SocketInfoService } from './../../../../app-services/socket-info.service';
import { KeyframesService } from './../../../../app-services/keyframes.service';

import { RobotSocketComponent } from './../../../../app-components/robot-socket/robot-socket.component';

import * as $ from 'jquery';

@Component({
  selector: 'app-input-poses',
  templateUrl: './input-poses.component.html',
  styleUrls: ['./input-poses.component.css']
})
export class InputPosesComponent implements OnInit {

  @ViewChild(RobotSocketComponent)
  robotSocket: RobotSocketComponent;

  keyframes;
  user;
  time: any;
  message;
  socketInfo;
  @Output() newMessage = new EventEmitter<Object>();

  constructor(
    keyframes: KeyframesService,
    socketInfo: SocketInfoService,
    private utilityService: UtilityService
  ) {
    this.keyframes = keyframes;
    this.socketInfo = socketInfo;
   }

  ngOnInit() {
    this.socketInfo.destinyChanged.subscribe((value) => {
      this.socketInfo.robot_destiny = value;
    });

    this.user = JSON.parse(localStorage.getItem('user'));
  }

  onMessage(message) { // envio das msgs socket para o componente em que este foi 'chamado' (html)
    this.newMessage.emit(message);
  }

  tableAdd() {
    /*
      Adds pose to the table
    */
    const temp = $.extend({}, this.keyframes.joints);
    let i = this.keyframes.tjoints.push(temp);
    this.keyframes.tjoints[i - 1][9] = this.time;
  }

  private wsGetPose() {
  /*
      Requisição da pose atual do beo - aba 'live'
  */
    this.message = {action: 'get_pose', origin: this.user.id, destiny: this.socketInfo.robot_destiny, data: ''};
    this.newMessage.emit(this.message);
  }

  private wsPhysicalMirror() {
  /*
    Ativação do mirror dos braços no robô beo.
  */
    this.message = {action: 'enable_mirror', origin: this.user.id, destiny: this.socketInfo.robot_destiny, data: ''};
    this.newMessage.emit(this.message);
  }

  ngOnDestroy() {

  }

}
