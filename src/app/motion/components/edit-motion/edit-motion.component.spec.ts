import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMotionComponent } from './edit-motion.component';

describe('EditMotionComponent', () => {
  let component: EditMotionComponent;
  let fixture: ComponentFixture<EditMotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
