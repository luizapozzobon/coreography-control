
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { FlashMessagesService } from 'angular2-flash-messages';

import { SocketInfoService } from './../../../../app-services/socket-info.service';
import { PlayMotionService } from './../../../services/play-motion.service';
import { ValidateService } from './../../../../app-services/validate.service';
import { MotionService } from './../../../services/motion.service';
import { KeyframesService } from './../../../../app-services/keyframes.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-poses-table',
  templateUrl: './poses-table.component.html',
  styleUrls: ['./poses-table.component.css']
})
export class PosesTableComponent implements OnInit {

  user;
  motionService: MotionService;
  keyframes: KeyframesService;
  socketInfo;
  validateService;
  message;
  @Output() newMessage = new EventEmitter<Object>();
  name: String;
  original_name: String;
  old_creator: String;
  selection: any = [];

  constructor(
    keyframes: KeyframesService,
    socketInfo: SocketInfoService,
    motionService: MotionService,
    private playMotionService: PlayMotionService,
    validateService: ValidateService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessages: FlashMessagesService
  ) {
    this.keyframes = keyframes;
    this.motionService = motionService;
    this.validateService = validateService;
    this.socketInfo = socketInfo;
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));

    this.keyframes.jointsChanged.subscribe();
    this.keyframes.mirrorChanged.subscribe();

    this.route.params.subscribe((param: Params) => {
      // quando se quer editar um movimento, salva o nome original
      // caso o usuário queira trocá-lo, salvará como um novo movimento e o original é mantido
      this.name = param['file'];
      this.original_name = param['file'];
    });

    this.user = JSON.parse(localStorage.getItem('user'));

    if (this.name) {
      this.motionService.getMotion(this.name).subscribe(motion => {
        this.keyframes.tjoints = motion.data.poses;
        this.old_creator = motion.data.creator;
      },
      err => {
        console.log(err);
        return false;
      });
    }

  }

  tableSelection(e) {
    /*
     Selects pose(s) from the table
    */

    if (e.target.checked) {
      this.selection.push(e.target.value);
    } else {
      let index = this.selection.indexOf(e.target.value);
      if (index > -1) {
        this.selection.splice(index, 1);
      }
    }
  }

  tableCopy() {
    /*
     Copies selected pose(s) from table
    */

    for (let i in this.selection) {
      let temp = JSON.parse(JSON.stringify($.extend({}, this.keyframes.tjoints[this.selection[i]])));
      this.keyframes.tjoints.push(temp);
    }
  }

  tableDelete() {
    /*
      Deletes selected pose(s) from table
    */

    let sorted_selection = this.selection.sort(function (a, b) { return b - a; });
    for (let i in sorted_selection) {
      this.keyframes.tjoints.splice(sorted_selection[i], 1);
    }
    this.selection = [];
    let cbs = document.getElementsByTagName('input');
    for (let i = 0; i < cbs.length; i++) {
      cbs[i].checked = false;
    }
  }

  tablePlay(index: number) {
    /*
      Play specific pose
    */

    let temp = JSON.parse(JSON.stringify($.extend({}, this.keyframes.tjoints[index])));
    delete temp[9];
    if (this.socketInfo.robot_destiny) {
      this.keyframes.joints = temp;
      this.message = {origin: this.user.id, data: temp, destiny: this.socketInfo.robot_destiny, action: 'set_pose'};
      this.newMessage.emit(this.message);
    } else {
      this.keyframes.joints = temp;
    }
  }

  playMotion() {
    this.message = {data: this.keyframes.tjoints, destiny: this.socketInfo.robot_destiny, action: 'play_motion', origin: this.user.id};
    this.newMessage.emit(this.message);

    this.playMotionService.playMotion(this.keyframes.tjoints);
  }

  onMotionSave() {
    /*
      Saves or updates motion into a json file in the database.
    */
    let duration = 0;
    this.keyframes.tjoints.forEach(pose => {
      duration = duration + pose[9];
    });

    const motionInfo = {
      name: this.name,
      poses: this.keyframes.tjoints,
      creator: this.user.username,
      isAdmin: this.user.isAdmin,
      old_creator: this.old_creator,
      duration: duration
    };

  /*
    Verificação de autorização de atualização (admin ou user).
    Verificação de existência de movimento com o nome
  */
    this.motionService.findMotion(this.name).subscribe(data0 => {
      if (data0.success) {
        if (this.user.username == data0.creator) {
          this.motionService.updateMotion(motionInfo).subscribe(data1 => {
            if (data1.success) {
              this.flashMessages.show('Motion updated', {cssClass: 'alert-success', timeout: 3000});
            }
          });
        } else {
          this.flashMessages.show('A motion with that name already exists. Please pick another one.',
           {cssClass: 'alert-danger', timeout: 5000});
        }
      } else {
        this.motionService.saveMotion(motionInfo).subscribe(data2 => {
          if (data2.success) {
            this.flashMessages.show('Motion saved', {cssClass: 'alert-success', timeout: 3000});
          } else {
            this.flashMessages.show('Something went wrong.', {cssClass: 'alert-danger', timeout: 3000});
          }
        });
      }
    });
  }

  tableCancel() {
    /*
      Caso existe poses na tabela, aparecerá o pop-up na tela para confirmar saída do usuário da página.
      Para previnir saídas por acidente.
    */
    if (this.keyframes.tjoints.length >= 1) {
      if (confirm('Are you sure you want to leave?') === true) {
        this.router.navigate(['/motion']);
      }
    } else {
      this.router.navigate(['/motion']);
    }
  }

  moveUp(i) {
    /*
      This function and the next one (moveDown) are responsible for
      moving poses up and down the poses table through the arrows.
    */
    if (i > 0) {
      const temp = JSON.parse(JSON.stringify($.extend({}, this.keyframes.tjoints[i - 1])));
      this.keyframes.tjoints[i - 1] = this.keyframes.tjoints[i];
      this.keyframes.tjoints[i] = temp;
    }
  }

  moveDown(i) {
    if (i < this.keyframes.tjoints.length - 1) {
      const temp = JSON.parse(JSON.stringify($.extend({}, this.keyframes.tjoints[i + 1])));
      this.keyframes.tjoints[i + 1] = this.keyframes.tjoints[i];
      this.keyframes.tjoints[i] = temp;
    }
  }
}
