import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosesTableComponent } from './poses-table.component';

describe('PosesTableComponent', () => {
  let component: PosesTableComponent;
  let fixture: ComponentFixture<PosesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
