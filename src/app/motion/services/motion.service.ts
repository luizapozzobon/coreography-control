/*
  Serviço de requisição de dados do front ao back-end, todas relacionadas aos movimentos.
 */

import { AuthService } from './../../app-services/auth.service';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MotionService {

  constructor(
    private authService: AuthService,
    private http: Http) { }


  motionQuantity(username) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'motions/' + username + '/quantity', {headers: headers})
      .map(res => res.json());
  }

  saveMotion(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'motions/save', file, {headers: headers})
      .map(res => res.json());
  }

  updateMotion(data) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.patch(this.authService.ip + 'motions/' + data.name, data, {headers: headers})
      .map(res => res.json());
  }

  findMotion(data) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'motions/' + data + '/find', {headers: headers})
    .map(res => res.json());
  }

  getMotion(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'motions/' + file + '/edit', {headers: headers})
    .map(res => res.json());
  }

  getMotionSearch(search) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'motions/list/search', search, {headers: headers})
      .map(res => res.json());
  }

  getMotionsList() {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'motions/list', {headers: headers})
      .map(res => res.json());
  }

  downloadMotion(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'motions/' + file + '/download', {headers: headers})
    .map(res => res.json());
  }

  uploadMotion(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'motions/upload', file, {headers: headers})
    .map(res => res.json());
  }

  removeMotion(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.delete(this.authService.ip + 'motions/' + file, {headers: headers})
      .map(res => res.json());
  }
}
