import { SocketInfoService } from './../../app-services/socket-info.service';
import { DataTransferService } from './../../app-services/data-transfer.service';
import { KeyframesService } from './../../app-services/keyframes.service';
import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable()
export class PlayMotionService {

  keyframes;
  websocket;
  socketInfo;
  user;

  constructor(keyframes: KeyframesService,
    websocket: DataTransferService,
    socketInfo: SocketInfoService) {
    this.keyframes = keyframes;
    this.websocket = websocket;
    this.socketInfo = socketInfo;
    this.user = JSON.parse(localStorage.getItem('user'));
   }

  private executePlus(k, z, time_per_tick, factor, i) {
    /*
      This function and the executeMinus execute each joint movement, being that a
      crescent or a decrescent one, respecting its adding factor.
    */

    let until_time = z * time_per_tick; // multiplica pelo z para executar todos os movimentos no intervalo
    setTimeout(() => {
      this.keyframes.joints[k] = this.keyframes.joints[k] +  factor;
    }, until_time);
  }

  private executeMinus(k, z, time_per_tick, factor, i) {
    let until_time = z * time_per_tick;
    setTimeout(() => {
      this.keyframes.joints[k] = this.keyframes.joints[k] - factor;
    }, until_time);
  }

  play(i, time, tjoints) {
    /*
      Executes each joint movement in the beo render.
    */
    let k = 0;
    while (k < 9) {
      /* let dif = this.keyframes.tjoints[i][k] - this.keyframes.tjoints[i - 1][k]; */
      let dif = tjoints[i][k] - tjoints[i - 1][k];   // diferença entre posição da junta anterior e a atual (o quanto movimentará)
      if (dif > 0 && dif <= 3) { dif = 0; }          // arredondamentos para pequenas diferenças (considera como se não houvesse tido alteração)
      if (dif < 0) { dif = - dif; }                  // todas difs devem ser positivas
      const time_per_tick = time / dif;
      let factor = 1;

      // Cálculo do fator de correção para execução correta dos movimentos no navegador
      // HTML5 suporta no mínimo 4ms na função setTimeout
      if (time_per_tick <= 1 && time_per_tick !== Infinity) {
        factor = 50 / time_per_tick;
      } else if (time_per_tick <= 5) {
        factor = 45 / time_per_tick;
      } else if (time_per_tick <= 7.5) {
        factor = 40 / time_per_tick;
      } else if (time_per_tick <= 10) {
        factor = 35 / time_per_tick;
      } else if (time_per_tick <= 12.5) {
        factor = 30 / time_per_tick;
      } else if (time_per_tick <= 15) {
        factor = 25 / time_per_tick;
      } else if (time_per_tick <= 17.5) {
        factor = 23 / time_per_tick;
      } else if (time_per_tick <= 20) {
        factor = 20 / time_per_tick;
      }

      let z = 1; // variável para controle de quanto já foi movimentado na renderização (deve atingir o valor de 'dif')
      while (this.keyframes.joints[k] <= tjoints[i][k] && z < dif) {
        this.executePlus(k, z, time_per_tick, factor, i);
        z = z + factor;
      }
      while (this.keyframes.joints[k] >= tjoints[i][k] && z < dif) {
        this.executeMinus(k, z, time_per_tick, factor, i);
        z = z + factor;
      }

      k++;
    }
  }

  private playMotionTimeout(i: number, time, tjoints) {
    this.play(i, time, tjoints);
    return new Promise((resolve, reject) => // só passará à próxima pose quando esse timeout for completo
      setTimeout(() => {
        resolve('done');
      }, time) // time = tempo da pose atual
    );
  }

  async playMotion(tjoints) {
    /*
      Plays motion. Depends on the two previous functions:
      playMotionTimeout, play, executeMinus and executePlus
    */
    let message = {data: tjoints, destiny: this.socketInfo.robot_destiny, action: 'play_motion', origin: this.user.id};
    this.websocket.messages.next(message);
    let time = 0;
    this.keyframes.joints = JSON.parse(JSON.stringify($.extend({}, tjoints[0])));
    for (let i = 1; i < tjoints.length; i++) {    // iteração das poses do movimento em questão
      time = (tjoints[i - 1][9] * 1000);          // tempo da pose 0 em ms
      await this.playMotionTimeout(i, time, tjoints).then((resolve) => { // aguardo da promessa para avançar à próxima pose
        // console.log('carry on', i, resolve);
      });
    }
  }
}
