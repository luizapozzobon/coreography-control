import { TestBed, inject } from '@angular/core/testing';

import { PlayMotionService } from './play-motion.service';

describe('PlayMotionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlayMotionService]
    });
  });

  it('should be created', inject([PlayMotionService], (service: PlayMotionService) => {
    expect(service).toBeTruthy();
  }));
});
