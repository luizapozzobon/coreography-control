import { FlashMessagesModule } from 'angular2-flash-messages';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatTableModule, MatFormFieldModule, MatInputModule, MatTabsModule } from '@angular/material';

import { SharedModule } from './../shared/shared.module';

import { PosesTableComponent } from './components/edit-motion/poses-table/poses-table.component';
import { InputPosesComponent } from './components/edit-motion/input-poses/input-poses.component';
import { EditMotionComponent } from './components/edit-motion/edit-motion.component';
import { MotionDatabaseComponent } from './components/motion-database/motion-database.component';

import { PlayMotionService } from './services/play-motion.service';
import { MotionService } from './services/motion.service';

import { DataTransferService } from './../app-services/data-transfer.service';

import { AuthGuard } from './../guards/auth.guard';

const motionRoutes: Routes = [
  {path: '', component: MotionDatabaseComponent, canActivate: [AuthGuard]},
  {path: 'new', component: EditMotionComponent, canActivate: [AuthGuard]},
  {path: ':file/edit', component: EditMotionComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    RouterModule.forChild(motionRoutes),
    FlashMessagesModule.forRoot()
  ],
  declarations: [
    EditMotionComponent,
    PosesTableComponent,
    InputPosesComponent,
    MotionDatabaseComponent
  ],
  providers: [
    MotionService,
    PlayMotionService,
    DataTransferService
  ],
  exports: [
    MotionDatabaseComponent
  ]
})
export class MotionModule { }
