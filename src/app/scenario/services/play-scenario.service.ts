import { NgDragDropModule } from 'ng-drag-drop';
import { ScenarioService } from './scenario.service';
import { DataTransferService } from './../../app-services/data-transfer.service';
import { KeyframesService } from './../../app-services/keyframes.service';
import { MotionService } from './../../motion/services/motion.service';
import { PlayMotionService } from './../../motion/services/play-motion.service';
import { Injectable } from '@angular/core';
import { PlaySpeechService } from '../../speech/services/play-speech.service';
import { SpeechService } from '../../speech/services/speech.service';
import { SocketInfoService } from '../../app-services/socket-info.service';

@Injectable()
export class PlayScenarioService {
  playMotionService;
  playSpeechService;
  motionService;
  speechService;
  scenarioService;
  keyframes;
  socketInfo;
  user;
  playing;

  constructor(
    playMotionService: PlayMotionService,
    playSpeechService: PlaySpeechService,
    motionService: MotionService,
    speechService: SpeechService,
    scenarioService: ScenarioService,
    keyframes: KeyframesService,
    socketInfo: SocketInfoService,
    private websocket: DataTransferService
  ) {
    this.playMotionService = playMotionService;
    this.playSpeechService = playSpeechService;
    this.motionService = motionService;
    this.speechService = speechService;
    this.scenarioService = scenarioService;
    this.keyframes = keyframes;
    this.socketInfo = socketInfo;
    this.user = JSON.parse(localStorage.getItem('user'));
   }

   async playScenarioEach(items) {
    /*
      Gather and play each scenario item individually.
      (each for iteration is one item data gathering and execution).
      Has less send and execution delay than playScenario() function.
    */
    this.playing = true;
    for (let item = 0; item < items.length; item++) {
      if (this.playing === true) {
        if (items[item].type === 'motion') {
          let r1 = await this.getMotionData(items[item].name);
          this.playMotionService.playMotion(r1['data']);
        } else if (items[item].type === 'speech') {
          let robot_play = true;
          this.playSpeechService.playSpeech(items[item]._id, robot_play);
        } else if (items[item].type === 'wheels') {
          this.rotateWheel(items[item]);
        } else if (items[item].type === 'sleep') {
          let r4 = {action: 'sleep', data: items[item].ms, origin: this.user.id};
          await this.sleep(r4.data);
        } else if (items[item].type === 'eye') {
          this.setEyes(items[item]);
        } else if (items[item].type === 'scenario') {
          let r5 = await this.getScenarioData(items[item].name);
          await this.playScenarioEach(r5);
        }
      }
    }
  }

   getMotionData(motion_name) {
    return new Promise((resolve, reject) => {
      this.motionService.getMotion(motion_name).subscribe(data => {
        let message = {action: 'play_motion', data: data.data.poses, type: 'motion'};
        resolve(message);
      });
    });
  }

  getSpeechData(speech_id) {
    return new Promise((resolve, reject) => {
      this.speechService.playSpeech(speech_id).subscribe(data2 => {
        let message = {action: 'play_speech', data: data2.data, type: 'speech'};
        resolve(message);
      });
    });
  }

  getScenarioData(scenario_name) {
    return new Promise((resolve, reject) => {
      this.scenarioService.getScenario(scenario_name).subscribe(data3 => {
        resolve(data3.scenario.items);
      });
    });
  }

  private rotateWheel(wheel) {
    let message = {action: 'move_wheels_scenario', data: wheel, destiny: this.socketInfo.robot_destiny, origin: this.user.id};
    this.websocket.messages.next(message);
    let tillTime = 0;
    const minTime = 10;
    const iteractions = wheel.duration / minTime;
    this.keyframes.wheelRotation.left = this.keyframes.wheelRotation.right = this.keyframes.wheelRotation.both = 0;
    while (tillTime <= wheel.duration) {
      setTimeout(() => {
        if (wheel.direction === 'forwards') {
          this.keyframes.wheelRotation[wheel.wheel]++;
          //console.log(this.keyframes.wheelRotation);
        } else if (wheel.direction === 'backwards') {
          this.keyframes.wheelRotation[wheel.wheel]--;
          //console.log(this.keyframes.wheelRotation);
        }
      }, tillTime);
      tillTime = tillTime + minTime;
    }
  }

  sleep(ms) {
    let message = {data: ms, action: 'sleep', destiny: this.socketInfo.robot_destiny, origin: this.user.id};
    this.websocket.messages.next(message);
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  setEyes(option) {
    this.keyframes.eyes = option.id;
    let message = {data: option, destiny: this.socketInfo.robot_destiny, action: 'set_expression', origin: this.user.id};
    this.websocket.messages.next(message);
  }


                        /* Unused but functional functions below */

  async getData (items) {
     /*
      Gathers all items data.
      Used in playScenario()
     */
    let scenario_data = [];
    let custom_id = 0;
    for (let item = 0; item < items.length; item++) {
      if (items[item].type === 'motion') {
        let r1 = await this.getMotionData(items[item].name);
        scenario_data.push(r1);
      } else if (items[item].type === 'speech') {
        let r2 = await this.getSpeechData(items[item]._id);
        scenario_data.push(r2);
      } else if (items[item].type === 'wheels') {
        let r3 = {action: 'rotate_wheels', data: items[item]};
        scenario_data.push(r3);
      } else if (items[item].type === 'sleep') {
        let r4 = {action: 'sleep', data: items[item].ms, origin: this.user};
        scenario_data.push(r4);
      } else if (items[item].type === 'eye') {
        let r5 = {action: 'set_expression', data: items[item]};
        scenario_data.push(r5)
      } else if (items[item].type === 'scenario') {
        let r6 = await this.getScenarioData(items[item].name);
        return new Promise((resolve, reject) => {
          resolve(this.getData(r6));
        });
      } else if (items[item].type === 'custom') {
        let r7 = {action: 'custom', data: '', id: custom_id};
        custom_id++;
        scenario_data.push(r7);
      }
    }
    return scenario_data;
  }

  async play(scenario_data) {
    /*
      Loops through scenario items in order to play them. All data are already collected.
      Used in playScenario().
    */
    for (let item = 0; item < scenario_data.length; item++) {
      if (scenario_data[item].action === 'play_motion') {
        console.log(scenario_data[item].data)
        this.playMotionService.playMotion(scenario_data[item].data);
      } else if (scenario_data[item].action === 'play_speech') {
        let robot_play = false;
        this.playSpeechService.prepareToPlay(scenario_data[item].data);
      } else if (scenario_data[item].action === 'sleep') {
        await this.sleep(scenario_data[item].data);
      } else if (scenario_data[item].action === 'rotate_wheels') {
        this.rotateWheel(scenario_data[item].data);
      } else if (scenario_data[item].action === 'set_expression') {
        this.setEyes(scenario_data[item].data);
      }
    }
  }

  playScenario(items) {
    /*
      Gathers all scenario data with getData(), then plays them with play().
      If the scenario is too big, this tends to have a big delay when sending to the robot.
    */
    let scenario_data = [];
    this.getData(items).then(data => {
     // if (this.socketInfo.robot_destiny) {
        let message = {destiny: this.socketInfo.robot_destiny, action: 'play_scenario',
        data: data, origin: this.user.id};
        this.websocket.messages.next(message);
      //}
      this.play(data);
    });
  }
}
