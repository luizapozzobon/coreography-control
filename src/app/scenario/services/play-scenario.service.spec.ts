import { TestBed, inject } from '@angular/core/testing';

import { PlayScenarioService } from './play-scenario.service';

describe('PlayScenarioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlayScenarioService]
    });
  });

  it('should be created', inject([PlayScenarioService], (service: PlayScenarioService) => {
    expect(service).toBeTruthy();
  }));
});
