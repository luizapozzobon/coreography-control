/*
  Serviço de requisições do front ao back-end, todos relacionados aos cenários
 */
import { AuthService } from './../../app-services/auth.service';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ScenarioService {

  constructor(
    private authService: AuthService,
    private http: Http) { }


  scenariosQuantity(username) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'scenarios/' + username + '/quantity', {headers: headers})
      .map(res => res.json());
  }

  saveScenario(scenario) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'scenarios/save', scenario, {headers: headers})
      .map(res => res.json());
  }

  updateScenario(scenario) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.patch(this.authService.ip + 'scenarios/' + scenario.name, scenario, {headers: headers})
      .map(res => res.json());
  }

  getScenario(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'scenarios/' + file + '/edit', {headers: headers})
    .map(res => res.json());
  }

  getScenariosList() {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'scenarios/list', {headers: headers})
      .map(res => res.json());
  }

  getScenarioSearch(search) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'scenarios/list/search', search, {headers: headers})
      .map(res => res.json());
  }

/*   findScenario(data) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.authService.ip + 'scenarios/' + data + '/find', {headers: headers})
    .map(res => res.json());
  } */

  removeScenario(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.delete(this.authService.ip + 'scenarios/' + file, {headers: headers})
      .map(res => res.json());
  }

  downloadScenario(file) {
    let headers = new Headers;
    this.authService.loadToken();
    headers.append('Authorization', this.authService.authToken);
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.authService.ip + 'scenarios/download', file, {headers: headers})
    .map(res => res.json());
  }

}
