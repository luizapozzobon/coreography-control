import { DataTransferService } from './../../../app-services/data-transfer.service';
import { SocketInfoService } from './../../../app-services/socket-info.service';
import { PlayScenarioService } from './../../services/play-scenario.service';
import { ScenarioService } from './../../services/scenario.service';
import { MatTableDataSource } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-scenario-database',
  templateUrl: './scenario-database.component.html',
  styleUrls: ['./scenario-database.component.css']
})
export class ScenarioDatabaseComponent implements OnInit {

  list;
  new_list;
  user;
  search;
  displayedColumns = ['options', 'name', 'creator'];
  socketInfo;
  websocket;

  constructor(
    private scenarioService: ScenarioService,
    private playScenarioService: PlayScenarioService,
    socketInfo: SocketInfoService,
    websocket: DataTransferService
  ) {
    this.socketInfo = socketInfo;
    this.websocket = websocket;
   }

  ngOnInit() {
    this.scenarioService.getScenariosList().subscribe(data => {
      this.list = new MatTableDataSource(data);
    });
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  applyFilter(filterValue: string) {
    // Aplicação do filtro de busca da tabela do banco de dados

    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.list.filter = filterValue;
  }

  deleteScenario(file: any) {
    const authorization = {
      scenario_creator: file.creator,
      current_user: this.user.username,
      isAdmin: this.user.isAdmin
    };

    if (authorization.scenario_creator === authorization.current_user || authorization.isAdmin === true) {
      this.scenarioService.removeScenario(file.name).subscribe(data => {
        this.scenarioService.getScenariosList().subscribe(data2 => {
          this.list = new MatTableDataSource(data2);
        });
      });
    } else {
      alert('Only the creator or an admin can remove a motion file.');
    }
  }

  onSearchChange() {
    this.scenarioService.getScenarioSearch({search: this.search}).subscribe(data => {
      this.new_list = data.data;
    });
  }

  trackMotions(index, file) {
    return file ? file.name : undefined;
  }

  generateDownloadUri(scenario) {
    this.scenarioService.getScenario(scenario).subscribe( (data) => {
      this.playScenarioService.getData(data.scenario.items).then(retrieved => {
        let message = {action: 'download_scenario', origin: this.user.id,
          destiny: this.socketInfo.robot_destiny, data: {filename: scenario, items: retrieved}};
        this.websocket.messages.next(message);
        this.scenarioService.downloadScenario(retrieved).subscribe(to_download => {
          let download_file = to_download.data;
          let a = document.createElement('a');
          a.setAttribute('style', 'display:none;');
          document.body.appendChild(a);
          let url = window.URL.createObjectURL(new Blob([download_file]));
          a.href = url;
          a.download = scenario + '.sbeo';
          a.click();
        });
      });
    });
  }

}
