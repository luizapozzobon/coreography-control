import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioDatabaseComponent } from './scenario-database.component';

describe('ScenarioDatabaseComponent', () => {
  let component: ScenarioDatabaseComponent;
  let fixture: ComponentFixture<ScenarioDatabaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScenarioDatabaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
