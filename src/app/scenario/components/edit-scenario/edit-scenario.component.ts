import { FlashMessagesService } from 'angular2-flash-messages';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UtilityService } from './../../../shared/services/utility.service';
import { ScenarioService } from './../../services/scenario.service';
import { PlaySpeechService } from './../../../speech/services/play-speech.service';
import { PlayMotionService } from './../../../motion/services/play-motion.service';
import { PlayScenarioService } from './../../services/play-scenario.service';
import { KeyframesService } from './../../../app-services/keyframes.service';
import { MotionService } from './../../../motion/services/motion.service';
import { SpeechService } from './../../../speech/services/speech.service';

import * as $ from 'jquery';

@Component({
  selector: 'app-edit-scenario',
  templateUrl: './edit-scenario.component.html',
  styleUrls: ['./edit-scenario.component.css']
})
export class EditScenarioComponent implements OnInit {

  description;
  speechToDrop;
  motionToDrop;
  scenarioToDrop;
  selectedWheel;
  selectedDirection;
  rotationTime;
  wheelsToDrop = [
    {wheel: 'left', direction: 'forwards', duration: 0, type: 'wheels'},
    {wheel: 'right', direction: 'forwards', duration: 0,  type: 'wheels'},
    {wheel: 'both', direction: 'forwards', duration: 0,  type: 'wheels'}];
  direction = ['forwards', 'backwards'];

  eyes = [{option: 'Default', id: 0, type: 'eye'}, {option: 'Mad', id: 1, type: 'eye'},
  {option: 'Sad', id: 2, type: 'eye'}, {option: 'Wink', id: 3, type: 'eye'}, {option: 'Shut', id: 4, type: 'eye'}];

  custom = {type: 'custom', name: 'Custom'};
  customCount = 0;

  itemsDropped = new Array();
  sleepTime;

  name;
  user;

  original_name;
  old_creator;

  constructor(
    private speechService: SpeechService,
    private motionService: MotionService,
    private scenarioService: ScenarioService,
    private playSpeechService: PlaySpeechService,
    private playMotionService: PlayMotionService,
    private playScenarioService: PlayScenarioService,
    private keyframes: KeyframesService,
    private utilityService: UtilityService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessages: FlashMessagesService
  ) { }

  ngOnInit() {

    this.user = JSON.parse(localStorage.getItem('user'));

    this.speechService.getSpeechList().subscribe(data => {
      this.speechToDrop = data.list;
      this.speechToDrop.forEach(element => {
        element.type = 'speech';    // tipo do elemento é fundamental para separação e aquisição dos dados
      });
    });
    this.motionService.getMotionsList().subscribe(data => {
      this.motionToDrop = data;
      this.motionToDrop.forEach(element => {
        element.type = 'motion';
      });
    });

    this.scenarioService.getScenariosList().subscribe(data => {
      this.scenarioToDrop = data;
      this.scenarioToDrop.forEach(element => {
        element.type = 'scenario';
      });
    });

    this.route.params.subscribe((param: Params) => {
      this.name = param['file'];
      this.original_name = param['file'];
    });

    if (this.name) {
      this.scenarioService.getScenario(this.name).subscribe(data => {
        this.itemsDropped = data.scenario.items;
        this.old_creator = data.scenario.creator;
      },
      err => {
        console.log(err);
        return false;
      });
    }
  }

  newScenario() {
    this.itemsDropped = new Array();
    this.keyframes.joints = this.keyframes.default_joints;
    this.customCount = 0;
  }

  updateWheelInfo() {
    /*
      Atualização das informações de movimentação das rodas alteradas no cartão antes de 'droppar' no scenario.
      Direção e tempo.
    */
    this.wheelsToDrop[this.selectedWheel].direction = this.direction[this.selectedDirection];
    this.wheelsToDrop[this.selectedWheel].duration = this.rotationTime;
  }

  addDropItem(event) {
    const cont = 0;
    if (typeof(event) === 'number') {
      event = {ms: event, type: 'sleep'};
    } else if (event.wheel) {
    } else if (event.type === 'eye') {
    } else if (event.type === 'custom') {
      event.name = 'Custom' + String(this.customCount);
      this.customCount++;
    } else if (event.type === 'scenario') {

    }

    this.itemsDropped.push($.extend({}, event)); // para todos os items arrastados

    if (event.type === 'speech') {      // para adicionar o sleep após o speech (tempo do speech)
      this.speechService.playSpeech(event._id).subscribe(data => {
        let speech = this.playSpeechService.toArrayBuffer(data.data);
        var self = this;
        this.playSpeechService.context.decodeAudioData(speech, function(buffer) {
          self.itemsDropped.push($.extend({}, {ms: Math.ceil(buffer.duration * 1000), type: 'sleep'}));
        });
      });
    }
  }

  private deleteDroppedItem(i) {
    this.itemsDropped.splice(i, 1);
  }

  playScenario() {
    /*
      Execução do cenário completo.
    */
    this.playScenarioService.playScenarioEach(this.itemsDropped);
    // this.playScenarioService.playScenario(this.itemsDropped);
  }

  stopScenario() {
    /*
      Para parar a execução do cenário. Espera finalizar o último item executado
    */
    this.playScenarioService.playing = false;
  }

  saveScenario() {
    if (this.name != '') {
      let file = {    // campos enviados ao back-end para salvar o arquivo
        name: this.name,
        items: this.itemsDropped,
        creator: this.user.username,
        old_creator: this.old_creator,
        description: this.description
      };

      // Verificação de autorização de salvamento/atualização do cenário.
      this.scenarioService.getScenario(this.name).subscribe(data => {
        if (data.success) {
          if (this.user.username === data.creator) {
            this.scenarioService.updateScenario(file).subscribe(data1 => {
              if (data1.success) {
                this.flashMessages.show('Scenario updated', {cssClass: 'alert-success', timeout: 3000});
              }
            });
          } else {
            this.flashMessages.show('A scenario with that name already exists. Please pick another one.',
              {cssClass: 'alert-danger', timeout: 3000});
          }
        } else {
          this.scenarioService.saveScenario(file).subscribe(data2 => {
            if (data2.success) {
              this.flashMessages.show('Scenario saved.', {cssClass: 'alert-success', timeout: 3000});
            } else {
              this.flashMessages.show('Something went wrong.', {cssClass: 'alert-danger', timeout: 3000});
            }
          });
        }
      });
    }
  }

  moveUp(i) {
    /*
      This function and the next one (moveDown) are responsible for
      moving poses up and down the poses table through the arrows.
    */
    if (i > 0) {
      const temp = JSON.parse(JSON.stringify($.extend({}, this.itemsDropped[i - 1])));
      this.itemsDropped[i - 1] = this.itemsDropped[i];
      this.itemsDropped[i] = temp;
    }
  }

  moveDown(i) {
    if (i < this.itemsDropped.length - 1) {
      const temp = JSON.parse(JSON.stringify($.extend({}, this.itemsDropped[i + 1])));
      this.itemsDropped[i + 1] = this.itemsDropped[i];
      this.itemsDropped[i] = temp;
    }
  }

  ngOnDestroy() {
    this.utilityService.wsCloseConnection(); // desliga a conexão com o robô ao deixar a tela
    this.keyframes.tjoints = [];             // esvazia a lista de poses
    this.keyframes.joints = this.keyframes.default_joints; // renderização do beo volta ao valor padrão das juntas
  }
}
