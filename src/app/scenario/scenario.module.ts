import { FlashMessagesModule } from 'angular2-flash-messages';

import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from './../shared/shared.module';

import { MatTableModule, MatInputModule, MatFormFieldModule, MatCardModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { NgDragDropModule } from 'ng-drag-drop';

import { EditScenarioComponent } from './components/edit-scenario/edit-scenario.component';
import { ScenarioDatabaseComponent } from './components/scenario-database/scenario-database.component';

import { SpeechService } from './../speech/services/speech.service';
import { PlaySpeechService } from './../speech/services/play-speech.service';
import { PlayMotionService } from './../motion/services/play-motion.service';
import { PlayScenarioService } from './services/play-scenario.service';
import { ScenarioService } from './services/scenario.service';

import { AuthGuard } from './../guards/auth.guard';

const scenarioRoutes: Routes = [
  {path: '', component: ScenarioDatabaseComponent,  canActivate: [AuthGuard]},
  {path: 'new', component: EditScenarioComponent,  canActivate: [AuthGuard]},
  {path: ':file/edit', component: EditScenarioComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatSelectModule,
    MatTooltipModule,
    NgDragDropModule.forRoot(),
    RouterModule.forChild(scenarioRoutes),
    FlashMessagesModule.forRoot()
  ],
  declarations: [
    EditScenarioComponent,
    ScenarioDatabaseComponent
  ],
  providers: [
    SpeechService,
    PlaySpeechService,
    PlayMotionService,
    ScenarioService,
    PlayScenarioService
  ]
})
export class ScenarioModule { }
