/* Back-end server */

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');


mongoose.Promise = global.Promise;
// Connect to database
mongoose.connect(config.database, {useMongoClient: true});

// On Connection
mongoose.connection.on('connected', () => {
  console.log('Connected to database '+config.database);
});
mongoose.connection.on('disconnected', () => {
  console.log('disconnected to db')
})

// On Error
mongoose.connection.on('error', (err) => {
  console.log('Database error: '+err);
});

mongoose.set('debug', true);

const app = express();
//const port = 3000;
const port = 21744;
// heroku port
//const port = process.env.PORT;

const users = require('./routes/users');
const motions = require('./routes/motions');
const speeches = require('./routes/speeches');
const scenarios = require('./routes/scenarios');

// CORS Middleware
app.use(cors()); // request to api from different domain name

// Set Static Folder
app.use(express.static(path.join(__dirname, 'dist')));
console.log(__dirname)

// Body Parser Middleware
app.use(bodyParser.json({limit:'50mb'}));

// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users)
app.use('/motions', motions)
app.use('/speeches', speeches)
app.use('/scenarios', scenarios)

// Index Route
app.get('/', (req, res) => {
  res.send('Invalid Endpoint, try again');
})

app.get('/*', function(req,res) {

  res.sendFile(path.join(__dirname+'/dist/index.html'));
});

// Qualquer outro route digitado leva para index
/* app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
}) */

// Start Server
app.listen(port, '0.0.0.0', () => {
  console.log('Server started on port '+port);
})
