import websocket
import ssl
from threading import Thread
import thread
import time
import json
import signal
import sys
import numpy as np
import io
import pickle
from pydub import AudioSegment
from pydub.playback import play

import subprocess as sp

from Movements import Movements
sys.path.append('/home/pi/beo/Modules/Movements')
sys.path.append('/home/pi/beo/Tools/motionEditor')
from motion import Motion
from Wheels import Wheels
from AsyncSerial import AsyncSerial
from Eyes import Eyes
from Audio import Audio
from copy import deepcopy

from functools import wraps

serial = AsyncSerial()
movements = Movements(serial)
motion = Motion()
wheels = Wheels(serial)
#eyes = Eyes()
audio = Audio()

joints_min = movements.joints_min
joints_max = movements.joints_max
angles_max = movements.angles_max

begin = True

robot_tag = 'beo008'

FFMPEG_BIN = 'ffmpeg'

#-----#

def jsonJoints(jointsList):
    jjoints = dict()
    for index in range(9):
        jjoints[index] = jointsList[index]
    return jjoints

def unicode_to_dict(moveList):
    move = []
    for i in moveList:
        pose = {}
        for j in i.keys():
            pose[int(j)] = i[j]
        move.append(pose)
    return move

#-----#

def run_async(func):
	@wraps(func)
	def async_func(*args, **kwargs):
		while True:
			try:
				func_h1 = Thread(target = func, args = args, kwargs = kwargs)
				func_h1.start()
			except:
				print('aaaaaaaaaaaaaaaaaaaaaaaaaa')
				print('{!r}; restarting thread')
			else:
				print('exited normally')
			return func_h1
	return async_func

def get_pose(origin):
	msg = movements.get_pose()
	ws.send(json.dumps({'action': 'joints_read', 'data': msg, 'destiny': origin, 'origin': robot_tag}))

def set_pose(data):
	pose = dict()
	for i in data.keys():
		pose[int(i)] = data[i]
	movements.set_pose(pose)

def play_motion(data):
	motion.keyframes = unicode_to_dict(data)
	print('Playing Motion')
	movements.play_motion(motion)

def play_motion_thread(data, sla):
	print('motion')
	motion.keyframes = unicode_to_dict(data)
	print('Playing Motion')
	try:
		movements.play_motion(motion)
		#return False
	except:
		pass

def disable_torque():
	print('Disable torque')
	movements.disable_all_joints()

def set_joint(data):
	i = data['index']
	movements.set_joint(i, data['value'][str(i)])

def enable_mirror():
	for i in range(3):
		angle = movements.get_raw_angle(i*2)
		movements.set_raw_angle(i*2 + 1, angle)

def move_wheels(direction):
	if(direction == "left" or direction == "forward_left"):
		wheels.move(10)
		wheels.move_wheel_left(12)
	elif(direction == "right" or direction == "forward_right"):
		wheels.move(10)
		wheels.move_wheel_right(12)
	elif(direction == "backward_left"):
		wheels.move(10)
		wheels.move_wheel_left(8)
	elif(direction == "backward_right"):
		wheels.move(10)
		wheels.move_wheel_right(8)
	elif(direction == "forwards"):
		wheels.move(12)
	elif(direction == "backwards"):
		wheels.move(8)

def stop_wheels(direction):
	wheels.move(10)
	
def move_wheels_scenario(data, sla):
        direction = 10
        print('WHEEL PLAYING', data)
        if(data["direction"] == "forwards"):
                direction = 12
        elif(data["direction"] == "backwards"):
                direction = 8
    
        if(data['wheel'] == "left"):
                wheels.move_wheel_left(direction)
        elif(data['wheel'] == "right"):
                wheels.move_wheel_right(direction)
        elif(data["wheel"] == "both"):
                wheels.move(direction)
                
        time.sleep(data["duration"] / 1000)
	
	try:
                wheels.move(10)
                print('Wheels Stopped')
	except:
		pass

def set_expression(expression):
	print(expression.lower())
	eyes.set_expression(expression.lower())
	
def play_scenario(scenario):
	for item in scenario:
		print("-------------------- item ---------------------")
		if(item["action"] == "play_motion"):
			thread.start_new_thread(play_motion_thread, (item["data"], 0))
		if(item["action"] == "play_speech"):
			thread.start_new_thread(play_speech_thread, (item["data"], 0))
		if(item["action"] == "sleep"):
			sleep_scenario(item["data"])
		if(item["action"] == "eyes"):
			set_expression(item["data"])

def sleep_scenario(ms):
	print('sleep ', ms)
	time.sleep(ms/1000)

def play_speech_thread(speech, sla):
	print('speech')
	a = np.array(speech, dtype="uint8")
	s = AudioSegment.from_file(io.BytesIO(a), format="mp3")
	play(s)

def play_speech(speech):
	a = np.array(speech, dtype="uint8")
	s = AudioSegment.from_file(io.BytesIO(a), format="mp3")
	play(s)

def download_motion(data):
	print('Motion downloaded')
	thefile = open('./motion/' + data["filename"] + '.beo', 'w')
	pickle.dump(data["poses"], thefile)
	thefile.close()

def download_scenario(data):
	print('Scenario downloaded')
	thefile = open('./scenarios/'+ data["filename"] + ".sbeo", 'w')
	pickle.dump(data["items"], thefile)
	thefile.close()
	
def download_speech(data):
	speech = np.asarray(data["data"]["data"], dtype=np.uint8)
	thefile = open('./audios/' + data["filename"] + '.mp3', 'w')
	thefile.write(speech)
	thefile.close()	

def set_button(data):
	print('set button')
	if (data["button"] == 'button1'):
		thefile = open('./button-config/' + 'button0.bbeo', 'w')
	elif (data["button"] == 'button2'):
		thefile = open('./button-config/' + 'button1.bbeo', 'w')
	elif (data["button"] == 'button3'):
		thefile = open('./button-config/' + 'button2.bbeo', 'w')
	elif (data["button"] == 'touch'):
		thefile = open('./button-config/' + 'touch.bbeo', 'w')
	
	try:
		pickle.dump(data["data"], thefile)
		thefile.close()
	except:
		pass


def on_message(ws, message):

	message = json.loads(message)
	print(message)

	if message["data"]:
		data = message["data"]
	else: 
		data = ""
	
	if message["action"]: 
		action = message["action"]
	else: 
		action = ""
	
	if message["origin"]: 
		origin = message["origin"]
	else: 
		origin = ""

	if(action == "ping"):
		ws.send(json.dumps({"action": "pong", "origin": robot_tag, "destiny": "server"}))

	if(action == "get_pose"):
		get_pose(origin)

	if(action == "set_pose"):
		set_pose(data)

	if(action == "play_motion"):
		#play_motion_thread(data, 0)
		thread.start_new_thread(play_motion_thread, (data, 0))
		#play_motion(data)

	if(action == "disable_torque"):
		disable_torque()

	if(action == "set_joint"):
		set_joint(data)

	if(action == "enable_mirror"):
		enable_mirror()

	if(action == 'move_wheels'):
		move_wheels(data)

	if(action == 'stop_wheels'):
		stop_wheels(data)

	if(action == 'move_wheels_scenario'):
		#move_wheels_scenario(data, 0)
		thread.start_new_thread(move_wheels_scenario, (data, 0))

	if(action == 'set_expression'):
		set_expression(data);

	if(action == 'play_scenario'):
		play_scenario(data)

	if(action == 'play_speech'):
		#play_speech_thread(data, 0)
		thread.start_new_thread(play_speech_thread, (data, 0))
                #play_speech(data)
		
	if(action == 'sleep'):
                sleep_scenario(data)

	if(action == "download_scenario"):
		download_scenario(data)

	if(action == "download_motion"):
		download_motion(data)

	if(action == "download_speech"):
		download_speech(data)

	if(action == "set_button"):
		set_button(data)

def on_error(ws, error):
    print(error)

def on_close(ws):
	print("### closed ###")

def on_open(ws):
    def run(*args):
    	if begin:
    		ws.send(json.dumps({"origin": robot_tag, "action": 'register_robot'}))
    	else:
		    ws.send(json.dumps({"origin": robot_tag, "data": 'data'}))
		    time.sleep(1)
		    ws.close()
		    print("thread terminating...")
    thread.start_new_thread(run, ())


def signal_handler(signal, frame):
	ws.send(json.dumps({"origin": robot_tag, "action": 'close_connection', "data": "robot"}))
	print ('You pressed Ctrl+C and closed connection!')
	sys.exit(0)

if __name__ == "__main__":
    audio.fala("Estou pronto")
    time.sleep(1) 
    websocket.enableTrace(True)
#    ws = websocket.WebSocketApp("ws://207.38.86.37:15315/",
#				on_message = on_message,
#				on_error = on_error,
#				on_close = on_close)
    ws = websocket.WebSocketApp("ws://192.168.0.101:15315/",
                             on_message = on_message,
                             on_error = on_error,
                             on_close = on_close)
    signal.signal(signal.SIGINT, signal_handler)
    print ('Press Ctrl+C in order to close connection!')
    ws.on_open = on_open
    ws.run_forever()   
# ws.run_forever(sslopt={"cert_reqs":ssl.CERT_NONE})
    
