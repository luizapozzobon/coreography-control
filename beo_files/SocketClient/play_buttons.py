import pickle
from PushButtons import PushButtons
from SensorTouch import SensorTouch
from time import sleep, clock

from scenario_reader2 import Scenario

pb = PushButtons()
S_TOUCH = SensorTouch()
LAST_TIME = clock()
touch = 0

s = Scenario()
touch = s.load('./button-config/touch.bbeo')
top = s.load('./button-config/top.bbeo')
mid = s.load('./button-config/mid.bbeo')
bottom = s.load('./button-config/bottom.bbeo')

try:
	while True:
		pb_pressed = pb.listen_buttons()
		
		if (clock() - LAST_TIME >= 0.05):
			LAST_TIME = clock()
			touch = S_TOUCH.get_status()
		if (pb_pressed != None):
			if(pb_pressed == 'TOP'): s.play_scenario(top)
			elif(pb_pressed == 'MID'): s.play_scenario(mid)
			elif(pb_pressed == 'BOT'): s.play_scenario(bot) 
		if (touch == 1):
			print('pressed')
			s.play_scenario(touch)
			sleep(1)
			m.disable_all_joints()
finally:
	print("End")
