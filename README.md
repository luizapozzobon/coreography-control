# CoreoControl

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4. And developed by Luiza Pozzobon and Gean Stein.

## Website Objectives

This website is used to create and control Qiron Robotics' robot Beo's motions, speeches and scenarios.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## In case of downloading to develop

In order for it to work, must have NodeJS, npm, angular CLI and MongoDB installed in your computer. Then, inside the main folder (which contains this ReadMe file) the command 'npm install' should download and install all of this website's packages. Those packages can be found in the 'package.json' file.

It's suggested to use Nodemon to run the back-end server (app.js file) when developing locally. Also, there should be a local MongoDB database, its IP address can be set in the 'config/database.js' file.
First, run the MongoDB database, then the back-end server and finally the 'ng serve'.

For further documentation, feel free to contact Qiron Robotics.
